package com.tinubu.document.generator.docx4j.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOCX;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.Reader;
import java.io.StringReader;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.skin.CssSkin;
import com.tinubu.document.generator.core.skin.Skin;

public class DocxSkin extends AbstractValue implements Skin {
   protected final DocumentRepository skinRepository;
   protected final Document skin;
   protected final CssSkin htmlSkin;

   protected DocxSkin(DocxSkinBuilder builder) {
      this.skinRepository = builder.skinRepository;
      this.skin = builder.skin;
      this.htmlSkin = builder.htmlSkin;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends DocxSkin> defineDomainFields() {
      return Fields
            .<DocxSkin>builder()
            .superFields((Fields<DocxSkin>) super.defineDomainFields())
            .field("htmlSkin", v -> v.htmlSkin)
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedSkinContentType() {
      return isTypeAndSubtypeEqualTo(value(APPLICATION_OOXML_DOCX));
   }

   @Getter
   @Override
   public Optional<DocumentRepository> skinRepository() {
      return nullable(skinRepository);
   }

   @Getter
   @Override
   public Optional<Document> skin() {
      return nullable(skin);
   }

   @Getter
   @Override
   public Optional<DocumentPath> logoId() {
      return optional();
   }

   @Getter
   @Override
   public Optional<DocumentPath> watermarkId() {
      return optional();
   }

   /**
    * Provides an optional, custom, CSS skin to generator specialized for HTML output type.
    *
    * @return HTML output skin
    */
   @Getter
   public Optional<CssSkin> htmlOutputSkin() {
      return nullable(htmlSkin);
   }

   /**
    * No-op {@link DocxSkin} instance.
    *
    * @return no-op skin
    */
   public static DocxSkin noopSkin() {
      class NoopSkin extends DocxSkin {
         protected NoopSkin() {
            super(new DocxSkinBuilder().finalizeBuild());
         }
      }

      return new NoopSkin();
   }

   public static DocxSkinBuilder reconstituteBuilder() {
      return new DocxSkinBuilder().reconstitute();
   }

   public static class DocxSkinBuilder extends DomainBuilder<DocxSkin> {
      private DocumentRepository skinRepository;
      private Document skin;
      private CssSkin htmlSkin;

      @Setter
      public DocxSkinBuilder skinRepository(DocumentRepository skinRepository) {
         this.skinRepository = skinRepository;
         return this;
      }

      @Setter
      public DocxSkinBuilder skin(Document skin) {
         this.skin = skin;
         return this;
      }

      public DocxSkinBuilder skin(DocumentPath skinId) {
         return skin(findSkinById(skinId));
      }

      public DocxSkinBuilder skinContent(String skinContent) {
         return skinContent(new StringReader(notNull(skinContent, "skinContent")));
      }

      public DocxSkinBuilder skinContent(Reader skinContent) {
         return skin(createDocxDocument("skin", skinContent));
      }

      @Setter
      public DocxSkinBuilder htmlSkin(CssSkin htmlSkin) {
         this.htmlSkin = htmlSkin;
         return this;
      }

      @Override
      @SuppressWarnings("unchecked")
      public DocxSkinBuilder finalizeBuild() {
         return super.finalizeBuild();
      }

      @Override
      public DocxSkin buildDomainObject() {
         return new DocxSkin(this);
      }

      protected Document createDocxDocument(String name, Reader content) {
         notBlank(name, "name");
         notNull(content, "content");

         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(name + "-" + Uuid
                                          .newNameBasedSha1Uuid(name)
                                          .stringValue()))
                                    .contentType(APPLICATION_OOXML_DOCX)
                                    .streamContent(content, UTF_8)
                                    .build());
      }

      private Document findSkinById(DocumentPath skinId) {
         notNull(skinId, "skinId");
         notNull(skinRepository, "skinRepository");

         return skinRepository
               .findDocumentById(skinId)
               .orElseThrow(() -> new IllegalArgumentException(String.format(
                     "Can't find '%s' skin in repository",
                     skinId.stringValue())));
      }

   }
}
