package com.tinubu.document.generator.docx4j.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.docx4j.skin.DocxSkin;

/**
 * General {@link WordprocessingMLPackage} processor.
 */
@FunctionalInterface
public interface PackageProcessor {

   /**
    * Processes specified package.
    *
    * @param wordPackage package to process
    *
    * @return new processed package instance
    */
   WordprocessingMLPackage process(WordprocessingMLPackage wordPackage,
                                   Template template,
                                   TextModel model,
                                   DocxSkin skin);

   default PackageProcessor compose(PackageProcessor before) {
      notNull(before, "before");
      return (WordprocessingMLPackage wordPackage, Template template, TextModel model, DocxSkin skin) -> process(
            before.process(wordPackage, template, model, skin),
            template,
            model,
            skin);
   }

   default PackageProcessor andThen(PackageProcessor after) {
      notNull(after, "after");
      return (WordprocessingMLPackage wordPackage, Template template, TextModel model, DocxSkin skin) -> after.process(
            process(wordPackage, template, model, skin),
            template,
            model,
            skin);
   }

   static PackageProcessor identity() {
      return (wordPackage, template, model, skin) -> wordPackage;
   }
}