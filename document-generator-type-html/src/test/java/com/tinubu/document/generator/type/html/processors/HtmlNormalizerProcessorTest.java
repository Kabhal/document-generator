package com.tinubu.document.generator.type.html.processors;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.GenericDocument;
import com.tinubu.document.generator.type.html.HtmlDocument;
import com.tinubu.document.generator.type.html.XhtmlDocument;

public class HtmlNormalizerProcessorTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testNormalizerWhenNominal() {
      Document html = htmlDocument(UTF_8);

      Document normalized = new HtmlNormalizerProcessor(false).process(html);

      assertThat(normalized.content().stringContent()).isEqualToIgnoringWhitespace("<!DOCTYPE html>"
                                                                                   + "<html>"
                                                                                   + "<head>"
                                                                                   + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                                                                                   + "<title></title>"
                                                                                   + "</head>"
                                                                                   + "<body></body>"
                                                                                   + "</html>");

      assertThat(normalized.getClass()).isEqualTo(HtmlDocument.class);
      assertThat(normalized.documentId()).isEqualTo(DocumentPath.of("test.html"));
      assertThat(normalized.metadata().documentPath()).isEqualTo(Paths.get("test.html"));
      assertThat(normalized.metadata().documentName()).isEqualTo("test.html");
      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=UTF-8"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.metadata().contentSize()).hasValue(146L);
      assertThat(normalized.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(normalized.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(normalized.content().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.content().contentSize()).hasValue(146L);
   }

   @Test
   public void testNormalizerWhenFailOnWarnings() {
      Document html = htmlDocument(UTF_8);

      assertThatExceptionOfType(DocumentAccessException.class)
            .isThrownBy(() -> new HtmlNormalizerProcessor(false, true, false).process(html))
            .withMessage("Normalization WARNING : (line 2 column 1) discarding unexpected <unknown>");
   }

   @Test
   public void testNormalizerWhenFailOnErrors() {
      Document html = htmlDocument(UTF_8);

      assertThatExceptionOfType(DocumentAccessException.class)
            .isThrownBy(() -> new HtmlNormalizerProcessor(false, false, true).process(html))
            .withMessage("Normalization ERROR : (line 2 column 1) <unknown> is not recognized!");
   }

   @Test
   public void testNormalizerWhenContentEncodingSet() {
      Document html = htmlDocument(US_ASCII);

      Document normalized = new HtmlNormalizerProcessor(false).process(html);

      assertThat(normalized.content().stringContent()).containsIgnoringWhitespaces("<!DOCTYPE html>",
                                                                                   "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=US-ASCII\">");

      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=US-ASCII"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(US_ASCII);
      assertThat(normalized.content().contentEncoding()).hasValue(US_ASCII);
   }

   @Test
   public void testNormalizerWhenContentEncodingSetWithDefaultEncoding() {
      Document html = htmlDocument(US_ASCII);

      Document normalized = new HtmlNormalizerProcessor(false, UTF_8).process(html);

      assertThat(normalized.content().stringContent()).containsIgnoringWhitespaces("<!DOCTYPE html>",
                                                                                   "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=US-ASCII\">");

      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=US-ASCII"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(US_ASCII);
      assertThat(normalized.content().contentEncoding()).hasValue(US_ASCII);
   }

   @Test
   public void testNormalizerWhenNoContentEncodingSet() {
      Document html = htmlDocument(null);

      Document normalized = new HtmlNormalizerProcessor(false).process(html);

      assertThat(normalized.content().stringContent()).containsIgnoringWhitespaces("<!DOCTYPE html>",
                                                                                   "<meta http-equiv=\"Content-Type\" content=\"text/html\">");

      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=UTF-8"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.content().contentEncoding()).hasValue(UTF_8);
   }

   @Test
   public void testNormalizerWhenNoContentEncodingSetWithDefaultEncoding() {
      Document html = htmlDocument(null);

      Document normalized = new HtmlNormalizerProcessor(false, US_ASCII).process(html);

      assertThat(normalized.content().stringContent()).containsIgnoringWhitespaces("<!DOCTYPE html>",
                                                                                   "<meta http-equiv=\"Content-Type\" content=\"text/html\">");

      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=US-ASCII"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(US_ASCII);
      assertThat(normalized.content().contentEncoding()).hasValue(US_ASCII);
   }

   @Test
   public void testNormalizerWhenNoContentEncodingSetWithMetadataContentEncodingAndDefaultEncoding() {
      Document html =
            DocumentBuilder.from(htmlDocument(null)).metadata(m -> m.contentType(ISO_8859_1)).build();

      Document normalized = new HtmlNormalizerProcessor(false, US_ASCII).process(html);

      assertThat(normalized.content().stringContent()).containsIgnoringWhitespaces("<!DOCTYPE html>",
                                                                                   "<meta http-equiv=\"Content-Type\" content=\"text/html\">");

      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=ISO-8859-1"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(ISO_8859_1);
      assertThat(normalized.content().contentEncoding()).hasValue(ISO_8859_1);
   }

   @Test
   public void testNormalizerAsContentProcessorWhenNominal() {
      Document html = htmlDocument(UTF_8);

      Document normalized = new HtmlNormalizerProcessor(false).contentProcessor().process(html);

      assertThat(normalized.content().stringContent()).isEqualToIgnoringWhitespace("<!DOCTYPE html>"
                                                                                   + "<html>"
                                                                                   + "<head>"
                                                                                   + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                                                                                   + "<title></title>"
                                                                                   + "</head>"
                                                                                   + "<body></body>"
                                                                                   + "</html>");

      assertThat(normalized.getClass()).isEqualTo(html.getClass());
      assertThat(normalized.documentId()).isEqualTo(DocumentPath.of("test.html"));
      assertThat(normalized.metadata().documentPath()).isEqualTo(Paths.get("test.html"));
      assertThat(normalized.metadata().documentName()).isEqualTo("test.html");
      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType("text/html;charset=UTF-8"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.metadata().contentSize()).hasValue(146L);
      assertThat(normalized.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(normalized.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(normalized.content().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.content().contentSize()).hasValue(146L);
   }

   @Test
   public void testNormalizerWhenXhtml() {
      Document html = htmlDocument(UTF_8);

      Document normalized = new HtmlNormalizerProcessor(true).process(html);

      assertThat(normalized.content().stringContent()).isEqualToIgnoringWhitespace(
            "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"
            + "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
            + "<head>"
            + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
            + "<title></title>"
            + "</head>"
            + "<body></body>"
            + "</html>");

      assertThat(normalized.getClass()).isEqualTo(XhtmlDocument.class);
      assertThat(normalized.documentId()).isEqualTo(DocumentPath.of("test.html"));
      assertThat(normalized.metadata().documentPath()).isEqualTo(Paths.get("test.html"));
      assertThat(normalized.metadata().documentName()).isEqualTo("test.html");
      assertThat(normalized.metadata().contentType()).hasValue(parseMimeType(
            "application/xhtml+xml;charset=UTF-8"));
      assertThat(normalized.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.metadata().contentSize()).hasValue(279L);
      assertThat(normalized.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(normalized.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(normalized.content().contentEncoding()).hasValue(UTF_8);
      assertThat(normalized.content().contentSize()).hasValue(279L);
   }

   protected Document htmlDocument(Charset encoding) {
      GenericDocument html = GenericDocument.open(DocumentPath.of("test.html"), encoding);

      String charset = nullable(encoding).map(e -> "; charset=" + e.name()).orElse("");

      StringBuilder content = new StringBuilder()
            .append("<!DOCTYPE html>\n")
            .append("<UNKNOWN>\n")
            .append("<html lang=\"en\">\n")
            .append("<head>\n")
            .append("<meta http-equiv=\"Content-Type\" content=\"text/html")
            .append(charset)
            .append("\">\n")
            .append("</head>\n")
            .append("<body>\n")
            .append("</body>\n")
            .append("</html>\n");

      html.write(content.toString().getBytes(UTF_8));
      html.finish();

      return html.readableContent();
   }

}