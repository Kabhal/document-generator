package com.tinubu.document.generator.type.html.processors.printer;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.document.generator.type.html.printer.PrettyTransformerHtmlPrinter;
import com.tinubu.document.generator.type.html.processors.HtmlDomProcessor;

public class PrettyTransformerHtmlPrinterTest extends BaseHtmlPrinterTest {

   @Test
   // FIXME Transformer is buggy, ignores whitespace in test.
   public void testPrinterWhenNominal() {
      HtmlDomProcessor processor = identityProcessor().printer(new PrettyTransformerHtmlPrinter());

      Document html = htmlDocument(UTF_8).loadContent();
      String output = html.process(processor).content().stringContent();

      assertThat(output).isEqualToIgnoringWhitespace("<html lang=\"en\">\n"
                                                     + "<head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                                                     + "</head>\n"
                                                     + "<body>\n"
                                                     + "</body>\n"
                                                     + "</html>");

   }

}