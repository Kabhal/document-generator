package com.tinubu.document.generator.type.html.processors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.Charset;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.GenericDocument;

public class HtmlDomProcessorTest {

   @Test
   public void testWhenNominal() {

      HtmlDomProcessor domProcessor = new HtmlDomProcessor() {
         @Override
         protected void updateDocument(org.w3c.dom.Document document) {
            Element elt = document.createElement("test");
            Node body = document.getElementsByTagName("body").item(0);
            body.appendChild(elt);
         }
      };

      String processed = htmlDocument(UTF_8).process(domProcessor).loadContent().content().stringContent();

      assertThat(processed).isEqualTo("<html lang=\"en\">\n"
                                      + "<head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                                      + "</head>\n"
                                      + "<body>\n"
                                      + "<test></test></body>\n"
                                      + "</html>");
   }

   protected Document htmlDocument(Charset encoding) {
      GenericDocument html = GenericDocument.open(DocumentPath.of("test.html"), encoding);

      StringBuilder content = new StringBuilder()
            .append("<!DOCTYPE html>\n")
            .append("<html lang=\"en\">\n")
            .append("<head>\n")
            .append("</head>\n")
            .append("<body>\n")
            .append("</body>\n")
            .append("</html>\n");

      html.write(content.toString().getBytes(UTF_8));
      html.finish();

      return html.readableContent();
   }

}