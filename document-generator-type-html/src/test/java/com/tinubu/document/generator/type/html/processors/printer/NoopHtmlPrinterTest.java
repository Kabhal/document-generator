package com.tinubu.document.generator.type.html.processors.printer;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.document.generator.type.html.printer.NoopHtmlPrinter;
import com.tinubu.document.generator.type.html.processors.HtmlDomProcessor;

public class NoopHtmlPrinterTest extends BaseHtmlPrinterTest {

   @Test
   public void testPrinterWhenNominal() {
      HtmlDomProcessor processor = identityProcessor().printer(new NoopHtmlPrinter());

      String output = htmlDocument(UTF_8).process(processor).content().stringContent();

      assertThat(output).isEmpty();
   }

}