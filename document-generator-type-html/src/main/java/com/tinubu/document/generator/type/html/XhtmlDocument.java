/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.document.generator.type.html;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.TypedDocument;
import com.tinubu.document.generator.type.html.processors.HtmlNormalizerProcessor;

/**
 * Specialized {@link Document} representing a {@code application/xhtml+xml} document.
 */
public class XhtmlDocument extends TypedDocument<XhtmlDocument> {

   /**
    * Line separator for {@link #writeLn(String)} operation.
    */
   protected static final String LINE_SEPARATOR = "\n";
   /**
    * Document Content-type.
    */
   protected static final MimeType CONTENT_TYPE = APPLICATION_XHTML;
   /**
    * Default content encoding.
    *
    * @implSpec Default should be deterministic here.
    */
   protected static final Charset DEFAULT_CONTENT_ENCODING = UTF_8;

   protected final boolean normalized;

   protected XhtmlDocument(XhtmlDocumentBuilder builder) {
      super(builder);
      this.normalized = nullable(builder.normalized, false);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends XhtmlDocument> defineDomainFields() {
      return Fields
            .<XhtmlDocument>builder()
            .superFields((Fields<XhtmlDocument>) super.defineDomainFields())
            .technicalField("normalized", v -> v.normalized)
            .build();
   }

   /**
    * Creates a new xhtml document from existing document. Source document metadata are not changed and must
    * match this document requirements.
    *
    * @param document existing document to base on
    * @param assumeContentType whether to assume document content-type if content-type is unknown or has
    *       no encoding
    *
    * @return new xhtml document
    */
   public static XhtmlDocument of(Document document, boolean assumeContentType) {
      notNull(document, "document");

      if (document instanceof HtmlDocument) {
         return of((HtmlDocument) document);
      } else if (document.metadata().simpleContentType().map(TEXT_HTML::equals).orElse(false)) {
         return of(HtmlDocument.of(document));
      } else {
         return new XhtmlDocumentBuilder()
               .reconstitute()
               .copy(document)
               .assumeContentType(assumeContentType, CONTENT_TYPE)
               .build();
      }
   }

   public static XhtmlDocument of(Document document) {
      return of(document, false);
   }

   /**
    * Creates a new xhtml document from existing html document. Source html document is normalized to xhtml.
    *
    * @param document existing html document to base on
    *
    * @return new xhtml document
    */
   public static XhtmlDocument of(HtmlDocument document) {
      notNull(document, "document");

      return XhtmlDocumentBuilder.from(new HtmlNormalizerProcessor(true).process(document)).build();
   }

   public static XhtmlDocument open(DocumentPath documentId,
                                    OutputStream contentOutputStream,
                                    Charset contentEncoding) {
      notNull(documentId, "documentId");

      return new XhtmlDocumentBuilder()
            .open(documentId,
                  contentOutputStream,
                  nullable(contentEncoding, DEFAULT_CONTENT_ENCODING),
                  CONTENT_TYPE)
            .build();
   }

   public static XhtmlDocument open(DocumentPath documentId, Charset contentEncoding) {
      return open(documentId, null, contentEncoding);
   }

   public static XhtmlDocument open(DocumentPath documentId, OutputStream contentOutputStream) {
      return open(documentId, contentOutputStream, null);
   }

   public static XhtmlDocument open(DocumentPath documentId) {
      return open(documentId, null, null);
   }

   public XhtmlDocument normalize(boolean failOnWarnings, boolean failOnErrors) {
      if (normalized) {
         return this;
      }
      return XhtmlDocumentBuilder
            .from(process(new HtmlNormalizerProcessor(true, failOnWarnings, failOnErrors)))
            .normalized(true)
            .build();
   }

   public XhtmlDocument normalize() {
      return normalize(false, false);
   }

   @Override
   public XhtmlDocument write(byte[] buffer, int offset, int length) {
      return super.write(buffer, offset, length);
   }

   @Override
   public XhtmlDocument write(byte[] buffer) {
      return super.write(buffer);
   }

   @Override
   public XhtmlDocument write(InputStream inputStream) {
      return super.write(inputStream);
   }

   @Override
   public XhtmlDocument write(String string) {
      return super.write(string);
   }

   @Override
   public XhtmlDocument write(Reader reader) {
      return super.write(reader);
   }

   @Override
   public XhtmlDocument write(Document document) {
      return super.write(document);
   }

   @Override
   public XhtmlDocument writeLn(String string) {
      return super.writeLn(string);
   }

   @Override
   public int read(byte[] buffer, int offset, int length) {
      return super.read(buffer, offset, length);
   }

   @Override
   public int read(byte[] buffer) {
      return super.read(buffer);
   }

   @Override
   public int read(OutputStream outputStream) {
      return super.read(outputStream);
   }

   @Override
   public int read(Writer writer) {
      return super.read(writer);
   }

   @Override
   public int read(Document document) {
      return super.read(document);
   }

   @Override
   public String readLn() {
      return super.readLn();
   }

   @Override
   public String lineSeparator() {
      return LINE_SEPARATOR;
   }

   @Override
   public XhtmlDocument finish() {
      return super.finish();
   }

   @Override
   protected InvariantRule<MimeType> hasValidContentType() {
      return isTypeAndSubtypeEqualTo(value(CONTENT_TYPE));
   }

   @Override
   public XhtmlDocumentBuilder documentBuilder() {
      return new XhtmlDocumentBuilder();
   }

   public static XhtmlDocumentBuilder reconstituteBuilder() {
      return new XhtmlDocumentBuilder().reconstitute();
   }

   public static class XhtmlDocumentBuilder
         extends TypedDocumentBuilder<XhtmlDocument, XhtmlDocumentBuilder> {

      private Boolean normalized;

      /**
       * Copy constructor for {@link XhtmlDocument}
       *
       * @param document document to copy
       *
       * @return xhtml document builder
       */
      public static XhtmlDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new XhtmlDocumentBuilder().reconstitute().copy(document);
      }

      @Override
      protected XhtmlDocumentBuilder copy(Document document) {
         return super
               .copy(document)
               .optionalChain(nullableInstanceOf(document, XhtmlDocument.class),
                              (b, d) -> b.normalized(d.normalized));
      }

      protected XhtmlDocumentBuilder assumeContentType(boolean assumeContentType, MimeType contentType) {
         return super.assumeContentType(assumeContentType, assumeContentType, contentType);
      }

      public XhtmlDocumentBuilder normalized(Boolean normalized) {
         ensureReconstitute();
         this.normalized = normalized;
         return this;
      }

      @Override
      public XhtmlDocument buildDomainObject() {
         return new XhtmlDocument(this);
      }

   }
}
