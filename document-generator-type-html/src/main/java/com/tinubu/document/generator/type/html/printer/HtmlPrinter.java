package com.tinubu.document.generator.type.html.printer;

import java.io.OutputStream;
import java.nio.charset.Charset;

import org.w3c.dom.Document;

/**
 * Printer for {@link Document}.
 */
@FunctionalInterface
public interface HtmlPrinter {

   /**
    * Prints document to specified output.
    *
    * @param document document to print
    * @param encoding document encoding
    * @param output output
    */
   void print(Document document, Charset encoding, OutputStream output);

   /**
    * Returns a printer that outputs original content as-is without extra formatting. However, some
    * differences can appear in output (e.g.: remove DOCTYPE tag, add META tag).
    */
   static HtmlPrinter defaultPrinter() {
      return new TransformerHtmlPrinter();
   }

   /**
    * Returns a printer that outputs nothing.
    */
   static HtmlPrinter noopPrinter() {
      return new NoopHtmlPrinter();
   }
}

