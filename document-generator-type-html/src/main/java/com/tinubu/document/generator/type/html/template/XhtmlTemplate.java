package com.tinubu.document.generator.type.html.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.io.Reader;
import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.core.template.TextTemplate;
import com.tinubu.document.generator.type.html.XhtmlDocument;

public class XhtmlTemplate extends TextTemplate {

   protected XhtmlTemplate(XhtmlTemplateBuilder builder,
                           Function<? super Document, ? extends Document> documentNormalizer) {
      super(builder.processTemplate(documentNormalizer::apply));
   }

   protected XhtmlTemplate(XhtmlTemplateBuilder builder) {
      this(builder, templateDocument -> XhtmlDocument.of(templateDocument).normalize());
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return withStrippedParameters(isIn(value(list(APPLICATION_XHTML, TEXT_HTML))));
   }

   public static XhtmlTemplateBuilder reconstituteBuilder() {
      return new XhtmlTemplateBuilder().reconstitute();
   }

   public static class XhtmlTemplateBuilder extends TextTemplateBuilder {

      public static XhtmlTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new XhtmlTemplateBuilder()
               .template(template.template())
               .templateRepository(template.templateRepository().orElse(null));
      }

      @SuppressWarnings("resource")
      public static XhtmlTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return XhtmlTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      protected XhtmlTemplateBuilder processTemplate(DocumentProcessor processor) {
         return (XhtmlTemplateBuilder) super.processTemplate(processor);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (XhtmlTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder template(Document template) {
         return (XhtmlTemplateBuilder) super.template(template);
      }

      @Override
      public XhtmlTemplateBuilder template(DocumentPath templateId) {
         return (XhtmlTemplateBuilder) super.template(templateId);
      }

      @Deprecated
      public XhtmlTemplateBuilder templateId(DocumentPath templateId) {
         return template(templateId);
      }

      @Override
      public XhtmlTemplateBuilder templateContent(String templateContent) {
         return (XhtmlTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      public XhtmlTemplateBuilder templateContent(Reader templateContent) {
         return (XhtmlTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder header(Document header) {
         return (XhtmlTemplateBuilder) super.header(header);
      }

      @Override
      public XhtmlTemplateBuilder headerContent(String header) {
         return (XhtmlTemplateBuilder) super.headerContent(header);
      }

      @Override
      public XhtmlTemplateBuilder headerContent(Reader header) {
         return (XhtmlTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public XhtmlTemplateBuilder footer(Document footer) {
         return (XhtmlTemplateBuilder) super.footer(footer);
      }

      @Override
      public XhtmlTemplateBuilder footerContent(String footer) {
         return (XhtmlTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public XhtmlTemplateBuilder footerContent(Reader footer) {
         return (XhtmlTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public XhtmlTemplate buildDomainObject() {
         return new XhtmlTemplate(this);
      }

      @Override
      public XhtmlTemplate build() {
         return (XhtmlTemplate) super.build();
      }

   }
}
