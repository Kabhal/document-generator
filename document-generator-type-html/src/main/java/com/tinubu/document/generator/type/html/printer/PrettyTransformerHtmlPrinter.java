package com.tinubu.document.generator.type.html.printer;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;

import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.tinubu.commons.ports.document.domain.DocumentAccessException;

public class PrettyTransformerHtmlPrinter implements HtmlPrinter {

   protected static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();

   protected static final int DEFAULT_INDENT_LENGTH = 2;

   protected final int indentLength;

   public PrettyTransformerHtmlPrinter(int indentLength) {
      this.indentLength = validate(indentLength, "indentLength", isPositive()).orThrow();
   }

   public PrettyTransformerHtmlPrinter() {
      this(DEFAULT_INDENT_LENGTH);
   }

   @Override
   public void print(Document document, Charset encoding, OutputStream output) {
      validate(document, "document", isNotNull())
            .and(validate(encoding, "encoding", isNotNull()))
            .and(validate(output, "output", isNotNull()))
            .orThrow();

      try {
         Transformer transformer = TRANSFORMER_FACTORY.newTransformer();

         transformer.setOutputProperty(OutputKeys.METHOD, "html");
         transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
         transformer.setOutputProperty(OutputKeys.ENCODING, encoding.name());

         transformer.setOutputProperty(OutputKeys.INDENT, "yes");
         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                                       String.valueOf(indentLength));

         transformer.transform(new DOMSource(document), new StreamResult(output));
      } catch (TransformerException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }

   }

}
