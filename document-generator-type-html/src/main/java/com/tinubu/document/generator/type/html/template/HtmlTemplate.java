package com.tinubu.document.generator.type.html.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;

import java.io.Reader;
import java.util.function.Function;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.core.template.TextTemplate;
import com.tinubu.document.generator.type.html.HtmlDocument;

public class HtmlTemplate extends TextTemplate {

   protected HtmlTemplate(HtmlTemplateBuilder builder,
                          Function<? super Document, ? extends Document> documentNormalizer) {
      super(builder.processTemplate(documentNormalizer::apply));
   }

   protected HtmlTemplate(HtmlTemplateBuilder builder) {
      this(builder, templateDocument -> HtmlDocument.of(templateDocument).normalize());
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return isTypeAndSubtypeEqualTo(value(TEXT_HTML));
   }

   public static HtmlTemplateBuilder reconstituteBuilder() {
      return new HtmlTemplateBuilder().reconstitute();
   }

   public static class HtmlTemplateBuilder extends TextTemplateBuilder {

      public static HtmlTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new HtmlTemplateBuilder()
               .template(template.template())
               .templateRepository(template.templateRepository().orElse(null));
      }

      @SuppressWarnings("resource")
      public static HtmlTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return HtmlTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      protected HtmlTemplateBuilder processTemplate(DocumentProcessor processor) {
         return (HtmlTemplateBuilder) super.processTemplate(processor);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (HtmlTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder template(Document template) {
         return (HtmlTemplateBuilder) super.template(template);
      }

      @Override
      public HtmlTemplateBuilder template(DocumentPath templateId) {
         return (HtmlTemplateBuilder) super.template(templateId);
      }

      @Deprecated
      public HtmlTemplateBuilder templateId(DocumentPath templateId) {
         return template(templateId);
      }

      @Override
      public HtmlTemplateBuilder templateContent(String templateContent) {
         return (HtmlTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      public HtmlTemplateBuilder templateContent(Reader templateContent) {
         return (HtmlTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder header(Document header) {
         return (HtmlTemplateBuilder) super.header(header);
      }

      @Override
      public HtmlTemplateBuilder headerContent(String header) {
         return (HtmlTemplateBuilder) super.headerContent(header);
      }

      @Override
      public HtmlTemplateBuilder headerContent(Reader header) {
         return (HtmlTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public HtmlTemplateBuilder footer(Document footer) {
         return (HtmlTemplateBuilder) super.footer(footer);
      }

      @Override
      public HtmlTemplateBuilder footerContent(String footer) {
         return (HtmlTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public HtmlTemplateBuilder footerContent(Reader footer) {
         return (HtmlTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public HtmlTemplate buildDomainObject() {
         return new HtmlTemplate(this);
      }

      @Override
      public HtmlTemplate build() {
         return (HtmlTemplate) super.build();
      }

   }
}
