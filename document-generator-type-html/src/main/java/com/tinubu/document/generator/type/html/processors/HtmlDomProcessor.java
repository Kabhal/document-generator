package com.tinubu.document.generator.type.html.processors;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.document.generator.type.html.HtmlDocument;
import com.tinubu.document.generator.type.html.XhtmlDocument;
import com.tinubu.document.generator.type.html.printer.HtmlPrinter;
import com.tinubu.document.generator.type.html.printer.PrettyTransformerHtmlPrinter;

/**
 * Generic content processor for HTML documents using a DOM parser.
 * When used as a {@link #contentProcessor()}, document content-type is not checked and HTML must be
 * parseable.
 */
public abstract class HtmlDomProcessor implements DocumentProcessor, DocumentContentProcessor {

   protected static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY =
         DocumentBuilderFactory.newInstance();
   protected static final Charset DEFAULT_ENCODING = UTF_8;
   protected static final HtmlPrinter DEFAULT_PRINTER = HtmlPrinter.defaultPrinter();

   protected abstract void updateDocument(Document document);

   protected Charset defaultEncoding;
   protected HtmlPrinter printer;

   /**
    * Creates HTML DOM processor. Default encoding is used as default encoding for input content, and for
    * output content if HTML does not specify {@code meta} tag.
    *
    * @param defaultEncoding Optional default content encoding when not provided, or {@code null}
    * @param printer HTML printer
    */
   protected HtmlDomProcessor(Charset defaultEncoding, HtmlPrinter printer) {
      this.printer = validate(printer, "printer", isNotNull()).orThrow();
      this.defaultEncoding = validate(defaultEncoding, "defaultEncoding", isNotNull()).orThrow();
   }

   /**
    * Creates HTML DOM processor. Default encoding is UTF-8.
    *
    * @param defaultEncoding Optional default content encoding when not provided, or {@code null}
    */
   public HtmlDomProcessor(Charset defaultEncoding) {
      this(defaultEncoding, DEFAULT_PRINTER);
   }

   /**
    * Creates HTML DOM processor. Default encoding is UTF-8.
    *
    * @param printer HTML printer
    */
   public HtmlDomProcessor(HtmlPrinter printer) {
      this(DEFAULT_ENCODING, printer);
   }

   /**
    * Creates HTML DOM processor. Default encoding is UTF-8.
    */
   public HtmlDomProcessor() {
      this(DEFAULT_ENCODING, DEFAULT_PRINTER);
   }

   public static HtmlDomProcessor format(Charset defaultEncoding, HtmlPrinter printer) {
      return new HtmlDomProcessor(defaultEncoding, printer) {
         @Override
         protected void updateDocument(Document document) { }
      };
   }

   public static HtmlDomProcessor format(Charset defaultEncoding) {
      return format(defaultEncoding, DEFAULT_PRINTER);
   }

   public static HtmlDomProcessor format(HtmlPrinter printer) {
      return format(DEFAULT_ENCODING, printer);
   }

   public static HtmlDomProcessor format() {
      return format(DEFAULT_ENCODING, DEFAULT_PRINTER);
   }

   /**
    * @deprecated Use {@link #printer(HtmlPrinter)} instead
    */
   @Deprecated
   public HtmlDomProcessor prettyPrint(boolean prettyPrint, int indentLength) {
      this.printer = prettyPrint ? new PrettyTransformerHtmlPrinter(indentLength) : DEFAULT_PRINTER;
      return this;
   }

   /**
    * @deprecated Use {@link #printer(HtmlPrinter)} instead
    */
   @Deprecated
   public HtmlDomProcessor prettyPrint() {
      return prettyPrint(true, 2);
   }

   /**
    * @deprecated Use {@link #printer(HtmlPrinter)} instead
    */
   @Deprecated
   public HtmlDomProcessor indentLength(int indentLength) {
      return prettyPrint(true, 2);
   }

   public HtmlDomProcessor defaultEncoding(Charset defaultEncoding) {
      this.defaultEncoding = defaultEncoding;
      return this;
   }

   public HtmlDomProcessor printer(HtmlPrinter printer) {
      this.printer = printer;
      return this;
   }

   @Override
   public DocumentContent process(DocumentContent documentContent) {
      return process(documentContent, defaultEncoding);
   }

   public DocumentContent process(DocumentContent documentContent, Charset defaultEncoding) {
      try {
         DocumentBuilder builder = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();

         try (InputStream is = documentContent.inputStreamContent()) {
            Charset inputEncoding = documentContent.contentEncoding().orElse(defaultEncoding);

            InputSource documentSource = new InputSource(is);
            documentSource.setSystemId("document.content");
            documentSource.setEncoding(inputEncoding.name());

            Document document = builder.parse(documentSource);

            updateDocument(document);

            if (documentContent instanceof InputStreamDocumentContent
                || documentContent instanceof LoadedDocumentContent) {
               try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
                  Charset encoding = documentEncoding(document).orElse(inputEncoding);

                  try {
                     printer.print(document, encoding, output);
                  } catch (Exception e) {
                     throw new DocumentAccessException(e.getMessage(), e);
                  }

                  return InputStreamDocumentContentBuilder
                        .from(documentContent)
                        .content(output.toByteArray(), encoding)
                        .build();
               }
            } else {
               throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                             documentContent.getClass().getName()));
            }

         }

      } catch (ParserConfigurationException | SAXException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Creates a new document with processed HTML content.
    * Source document must be {@link PresetMimeTypeRegistry#TEXT_HTML}, or
    * {@link PresetMimeTypeRegistry#APPLICATION_XHTML}.
    *
    * @return processed HTML document as {@link HtmlDocument} or {@link XhtmlDocument}
    *
    * @throws DocumentAccessException if a normalization warning or error occurs,
    *       or if an I/O error occurs while reading document content
    */
   @Override
   public com.tinubu.commons.ports.document.domain.Document process(com.tinubu.commons.ports.document.domain.Document document) {
      validate(document,
               "document",
               metadata(contentType(isTypeAndSubtypeEqualTo(value(TEXT_HTML)).orValue(isTypeAndSubtypeEqualTo(
                     value(APPLICATION_XHTML)))))).orThrow();

      Charset documentEncoding = document
            .content()
            .contentEncoding()
            .orElseGet(() -> document.metadata().contentEncoding().orElse(defaultEncoding));

      if (document.content() instanceof InputStreamDocumentContent
          || document.content() instanceof LoadedDocumentContent) {

         return com.tinubu.commons.ports.document.domain.Document.DocumentBuilder
               .from(document)
               .build()
               .process(d -> process(d, documentEncoding));

      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       document.content().getClass().getName()));
      }

   }

   protected com.tinubu.commons.ports.document.domain.Document process(com.tinubu.commons.ports.document.domain.Document document,
                                                                       Charset defaultEncoding) {
      return document.content(process(document.content(), defaultEncoding));
   }

   protected static Optional<Charset> documentEncoding(Document document) {
      return nullable(document.getXmlEncoding())
            .or(() -> nullable(document.getInputEncoding()))
            .map(Charset::forName);
   }

}
