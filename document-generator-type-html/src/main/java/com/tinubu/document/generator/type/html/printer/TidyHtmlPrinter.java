package com.tinubu.document.generator.type.html.printer;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.io.OutputStream;
import java.nio.charset.Charset;

import org.w3c.dom.Document;
import org.w3c.tidy.DOMDocumentImpl;
import org.w3c.tidy.Tidy;

/**
 * JTidy based HTML printer.
 *
 * @implNote This printer is not compatible with regular {@link Document} implementations.
 */
public class TidyHtmlPrinter implements HtmlPrinter {

   private final boolean xhtml;
   private final Tidy tidy;

   public TidyHtmlPrinter(boolean xhtml, Tidy tidy) {
      this.xhtml = xhtml;
      this.tidy = validate(tidy, isNotNull()).orThrow();

      this.tidy.setXHTML(xhtml);
   }

   public TidyHtmlPrinter(boolean xhtml) {
      this(xhtml, defaultTidy());
   }

   private static Tidy defaultTidy() {
      Tidy tidy = new Tidy();

      tidy.setTidyMark(false);
      tidy.setQuiet(true);
      tidy.setShowWarnings(false);
      tidy.setShowErrors(0);

      return tidy;
   }

   @Override
   public void print(Document document, Charset encoding, OutputStream output) {
      validate(document, "document", isNotNull())
            .and(validate(encoding, "encoding", isNotNull()))
            .and(validate(output, "output", isNotNull()))
            .orThrow();

      if (!(document instanceof DOMDocumentImpl)) {
         throw new IllegalArgumentException(String.format("Document must be an instance of '%s'",
                                                          DOMDocumentImpl.class.getName()));
      }

      tidy.setInputEncoding(encoding.name());
      tidy.setOutputEncoding(encoding.name());

      tidy.pprint(document, output);
   }

}
