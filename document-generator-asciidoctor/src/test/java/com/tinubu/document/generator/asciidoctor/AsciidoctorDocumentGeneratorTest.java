package com.tinubu.document.generator.asciidoctor;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_YAML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.document.generator.core.support.DebugUtils.dumpDocument;
import static com.tinubu.document.generator.core.support.DebugUtils.openDocument;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.asciidoctor.generator.AsciidoctorDocumentGenerator.AsciidoctorDocumentGeneratorBuilder;
import com.tinubu.document.generator.asciidoctor.skin.PdfThemeSkin.PdfThemeSkinBuilder;
import com.tinubu.document.generator.asciidoctor.template.AsciidocTemplate.AsciidocTemplateBuilder;
import com.tinubu.document.generator.core.environment.ConfigurableEnvironment.ConfigurableEnvironmentBuilder;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.DocumentGenerator;
import com.tinubu.document.generator.core.model.TextModel.TextModelBuilder;
import com.tinubu.document.generator.core.skin.CssSkin.CssSkinBuilder;
import com.tinubu.document.generator.flyingsaucer.generator.FlyingSaucerDocumentGenerator.FlyingSaucerDocumentGeneratorBuilder;

public class AsciidoctorDocumentGeneratorTest {

   @Test
   public void generateHtmlDocument() throws DocumentGenerationException {
      DocumentGenerator generator = new AsciidoctorDocumentGeneratorBuilder(environment())
            .automaticLayoutDocumentSpecification(DocumentEntryCriteriaBuilder
                                                        .ofDocumentPaths(Paths.get(
                                                              "images/sample-watermark.png"))
                                                        .or(DocumentEntryCriteriaBuilder
                                                                  .ofContentTypes(TEXT_CSS)
                                                                  .and(new DocumentEntryCriteriaBuilder()
                                                                             .documentPathDepth(
                                                                                   CriterionBuilder.equal(0))
                                                                             .build())))
            .build();

      try (var document = generator
            .generateFromTemplate(new AsciidocTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("document.adoc"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  new CssSkinBuilder()
                                        .skinRepository(skinRepository())
                                        .skin(DocumentPath.of("styles.css"))
                                        .watermarkId(DocumentPath.of("images/sample-watermark.png"))
                                        .build())
            .loadDocumentContent()) {

         dumpDocument(document);

         Document pdf = document.convert(new FlyingSaucerDocumentGeneratorBuilder().build()).document();

         openDocument(pdf);
      }
   }

   @Test
   public void generatePdfDocument() throws DocumentGenerationException {
      DocumentGenerator generator = new AsciidoctorDocumentGeneratorBuilder(environment())
            .outputType("pdf")
            .automaticLayoutDocumentSpecification(DocumentEntryCriteriaBuilder
                                                        .ofDocumentPaths(Paths.get(
                                                                               "images/sample-watermark.png"),
                                                                         Paths.get("images/sample-logo.png"))
                                                        .or(DocumentEntryCriteriaBuilder
                                                                  .ofContentTypes(APPLICATION_YAML)
                                                                  .and(new DocumentEntryCriteriaBuilder()
                                                                             .documentPathDepth(
                                                                                   CriterionBuilder.equal(0))
                                                                             .build())))
            .build();

      try (var document = generator
            .generateFromTemplate(new AsciidocTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("document.adoc"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  new PdfThemeSkinBuilder()
                                        .skinRepository(skinRepository())
                                        .skin(DocumentPath.of("theme.yml"))
                                        .logoId(DocumentPath.of("images/sample-logo.png"))
                                        .watermarkId(DocumentPath.of("images/sample-watermark.png"))
                                        .build())
            .loadDocumentContent()) {

         openDocument(document);
      }
   }

   private static Environment environment() {
      return ConfigurableEnvironmentBuilder
            .ofEnvironment(SystemEnvironment.ofSystemDefaults())
            .timeZone(ZoneId.of("Europe/Paris"))
            .locale(Locale.FRANCE)
            .documentEncoding(StandardCharsets.UTF_8)
            .build();
   }

   private static ClasspathDocumentRepository templateRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/asciidoctor"));
   }

   private static DocumentRepository skinRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/asciidoctor/skin"));
   }

   private Map<String, Object> dataModel() {
      Map<String, Object> model = new HashMap<>();
      model.put("user", "Test User");
      model.put("latestProduct", new Product("products/greenmouse.html", "Green Mouse"));
      return model;
   }

   /**
    * Product sample bean.
    */
   public static class Product {

      private final String url;
      private final String name;

      public Product(String url, String name) {
         this.url = url;
         this.name = name;
      }

      public String getUrl() {
         return url;
      }

      public String getName() {
         return name;
      }
   }
}