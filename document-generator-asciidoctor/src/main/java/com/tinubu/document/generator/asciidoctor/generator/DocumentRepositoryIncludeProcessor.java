package com.tinubu.document.generator.asciidoctor.generator;

import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.IncludeProcessor;
import org.asciidoctor.extension.PreprocessorReader;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Asciidoctor processor to reference documents from a pre-defined list of documents, or from repository, in
 * "include" directives.
 * Documents are searched in priority in pre-defined list.
 */
public class DocumentRepositoryIncludeProcessor extends IncludeProcessor {

   private final List<com.tinubu.commons.ports.document.domain.Document> includeDocuments;
   private final DocumentRepository includeDocumentRepository;
   private final Charset defaultEncoding;

   public DocumentRepositoryIncludeProcessor(Map<String, Object> config,
                                             List<com.tinubu.commons.ports.document.domain.Document> includeDocuments,
                                             DocumentRepository includeDocumentRepository,
                                             Charset defaultEncoding) {
      super(config);
      this.includeDocuments = noNullElements(includeDocuments, "includeDocuments");
      this.includeDocumentRepository = notNull(includeDocumentRepository, "includeDocumentRepository");
      this.defaultEncoding = notNull(defaultEncoding, "defaultEncoding");
   }

   public DocumentRepositoryIncludeProcessor(List<com.tinubu.commons.ports.document.domain.Document> includeDocuments,
                                             DocumentRepository includeDocumentRepository,
                                             Charset defaultEncoding) {
      this(new HashMap<>(), includeDocuments, includeDocumentRepository, defaultEncoding);
   }

   @Override
   public boolean handles(String target) {
      return target != null && !target.startsWith("uri:");
   }

   @Override
   public void process(Document document,
                       PreprocessorReader reader,
                       String target,
                       Map<String, Object> attributes) {
      final com.tinubu.commons.ports.document.domain.Document include;

      include = stream(includeDocuments)
            .filter(d -> d.documentId().equals(DocumentPath.of(target)))
            .findFirst()
            .or(() -> includeDocumentRepository.findDocumentById(DocumentPath.of(target)))
            .orElseThrow(() -> new IllegalArgumentException(String.format("Include '%s' not found", target)));

      reader.pushInclude(include.content().stringContent(defaultEncoding), target, target, 1, attributes);
   }
}
