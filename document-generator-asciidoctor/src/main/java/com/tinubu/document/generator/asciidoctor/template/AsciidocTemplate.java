package com.tinubu.document.generator.asciidoctor.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_ASCIIDOC;

import java.io.Reader;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.core.template.TextTemplate;

public class AsciidocTemplate extends TextTemplate {

   protected AsciidocTemplate(AsciidocTemplateBuilder builder) {
      super(builder);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends AsciidocTemplate> defineDomainFields() {
      return Fields
            .<AsciidocTemplate>builder()
            .superFields((Fields<AsciidocTemplate>) super.defineDomainFields())
            .build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedTemplateContentType() {
      return isTypeAndSubtypeEqualTo(value(TEXT_ASCIIDOC));
   }

   public static AsciidocTemplateBuilder reconstituteBuilder() {
      return new AsciidocTemplateBuilder().reconstitute();
   }

   public static class AsciidocTemplateBuilder extends TextTemplateBuilder {

      public static AsciidocTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new AsciidocTemplateBuilder()
               .template(template.template())
               .templateRepository(template.templateRepository().orElse(null));
      }

      public static AsciidocTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return AsciidocTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         return (AsciidocTemplateBuilder) super.templateRepository(templateRepository);
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder template(Document template) {
         return (AsciidocTemplateBuilder) super.template(template);
      }

      @Override
      public AsciidocTemplateBuilder template(DocumentPath templateId) {
         return (AsciidocTemplateBuilder) super.template(templateId);
      }

      @Deprecated
      public AsciidocTemplateBuilder templateId(DocumentPath templateId) {
         return template(templateId);
      }

      @Override
      public AsciidocTemplateBuilder templateContent(String templateContent) {
         return (AsciidocTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      public AsciidocTemplateBuilder templateContent(Reader templateContent) {
         return (AsciidocTemplateBuilder) super.templateContent(templateContent);
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder header(Document header) {
         return (AsciidocTemplateBuilder) super.header(header);
      }

      @Override
      public AsciidocTemplateBuilder header(DocumentPath headerId) {
         return (AsciidocTemplateBuilder) super.header(headerId);
      }

      @Override
      public AsciidocTemplateBuilder headerContent(String header) {
         return (AsciidocTemplateBuilder) super.headerContent(header);
      }

      @Override
      public AsciidocTemplateBuilder headerContent(Reader header) {
         return (AsciidocTemplateBuilder) super.headerContent(header);
      }

      @Override
      @Setter
      public AsciidocTemplateBuilder footer(Document footer) {
         return (AsciidocTemplateBuilder) super.footer(footer);
      }

      @Override
      public AsciidocTemplateBuilder footer(DocumentPath footerId) {
         return (AsciidocTemplateBuilder) super.footer(footerId);
      }

      @Override
      public AsciidocTemplateBuilder footerContent(String footer) {
         return (AsciidocTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public AsciidocTemplateBuilder footerContent(Reader footer) {
         return (AsciidocTemplateBuilder) super.footerContent(footer);
      }

      @Override
      public AsciidocTemplate buildDomainObject() {
         return new AsciidocTemplate(this);
      }

      @Override
      public AsciidocTemplate build() {
         return (AsciidocTemplate) super.build();
      }

   }
}
