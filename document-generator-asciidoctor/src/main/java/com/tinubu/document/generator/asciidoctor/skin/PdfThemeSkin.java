package com.tinubu.document.generator.asciidoctor.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_YAML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.skin.Skin;

public class PdfThemeSkin extends AbstractValue implements Skin {
   protected final DocumentRepository skinRepository;
   protected final Document skin;
   protected final DocumentPath logoId;
   protected final DocumentPath watermarkId;

   protected PdfThemeSkin(PdfThemeSkinBuilder builder) {
      this.skinRepository = builder.skinRepository;
      this.skin = builder.skin;
      this.logoId = builder.logoId;
      this.watermarkId = builder.watermarkId;
   }

   @SuppressWarnings("unchecked")
   public Fields<? extends PdfThemeSkin> defineDomainFields() {
      return Fields
            .<PdfThemeSkin>builder()
            .superFields((Fields<PdfThemeSkin>) super.defineDomainFields())
            .build();
   }

   @Getter
   @Override
   public Optional<DocumentRepository> skinRepository() {
      return nullable(skinRepository);
   }

   @Getter
   @Override
   public Optional<Document> skin() {
      return nullable(skin);
   }

   @Getter
   public Optional<DocumentPath> logoId() {
      return nullable(logoId);
   }

   @Getter
   public Optional<DocumentPath> watermarkId() {
      return nullable(watermarkId);
   }

   @Override
   public InvariantRule<MimeType> isSupportedSkinContentType() {
      return isTypeAndSubtypeEqualTo(value(APPLICATION_YAML));
   }

   /**
    * Provides automatic configuration for this theme with sensible defaults.
    *
    * @return attributes maps
    *
    * @implSpec The values should not be prefixed with {@code @} here as
    *       the decision is deferred to calling code.
    * @implSpec Automatic attributes should not override theme configuration, so, fo example, logo and
    *       watermark are only applied if no theme is present.
    */
   public Map<String, Object> automaticAttributes() {
      Map<String, Object> attributes = new LinkedHashMap<>();

      skin().ifPresentOrElse(skin -> {
         attributes.put("pdf-theme", skin.documentId().stringValue());
         attributes.put("pdf-themesdir", "{docdir}");
      }, () -> {
         logoId().ifPresent(logoId -> {
            attributes.put("title-logo-image",
                           "image:"
                           + logoId.stringValue()
                           + "[top=0px,float=right,align=center,pdfwidth=30%]");
         });

         watermarkId().ifPresent(watermarkId -> {
            for (String watermarkAttribute : list("page-foreground-image"))
               attributes.put(watermarkAttribute,
                              "image:" + watermarkId.stringValue() + "[fit=none,pdfwidth=70%]");
         });
      });

      return attributes;
   }

   /**
    * No-op {@link PdfThemeSkin} instance.
    *
    * @return no-op skin
    */
   public static PdfThemeSkin noopSkin() {
      class NoopSkin extends PdfThemeSkin {
         protected NoopSkin() {
            super(new PdfThemeSkinBuilder().finalizeBuild());
         }
      }

      return new NoopSkin();
   }

   public static PdfThemeSkinBuilder reconstituteBuilder() {
      return new PdfThemeSkinBuilder().reconstitute();
   }

   public static class PdfThemeSkinBuilder extends DomainBuilder<PdfThemeSkin> {
      protected DocumentRepository skinRepository;
      protected Document skin;
      protected DocumentPath logoId;
      protected DocumentPath watermarkId;

      @Setter
      public PdfThemeSkinBuilder skinRepository(DocumentRepository skinRepository) {
         this.skinRepository = skinRepository;
         return this;
      }

      @Setter
      public PdfThemeSkinBuilder skin(Document skin) {
         this.skin = skin;
         return this;
      }

      public PdfThemeSkinBuilder skin(DocumentPath skinId) {
         return skin(findSkinById(skinId));
      }

      public PdfThemeSkinBuilder skinContent(String skinContent) {
         return skinContent(new StringReader(notNull(skinContent, "skinContent")));
      }

      public PdfThemeSkinBuilder skinContent(Reader skinContent) {
         return skin(createYamlDocument("skin", skinContent));
      }

      @Setter
      public PdfThemeSkinBuilder logoId(DocumentPath logoId) {
         this.logoId = logoId;
         return this;
      }

      @Setter
      public PdfThemeSkinBuilder watermarkId(DocumentPath watermarkId) {
         this.watermarkId = watermarkId;
         return this;
      }

      @Override
      @SuppressWarnings("unchecked")
      public PdfThemeSkinBuilder finalizeBuild() {
         return super.finalizeBuild();
      }

      @Override
      public PdfThemeSkin buildDomainObject() {
         return new PdfThemeSkin(this);
      }

      protected Document createYamlDocument(String name, Reader content) {
         notBlank(name, "name");
         notNull(content, "content");

         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(name + "-" + Uuid
                                          .newNameBasedSha1Uuid(name)
                                          .stringValue()))
                                    .contentType(APPLICATION_YAML)
                                    .streamContent(content, UTF_8)
                                    .build());
      }

      private Document findSkinById(DocumentPath skinId) {
         notNull(skinId, "skinId");
         notNull(skinRepository, "skinRepository");

         return skinRepository
               .findDocumentById(skinId)
               .orElseThrow(() -> new IllegalArgumentException(String.format(
                     "Can't find '%s' skin in repository",
                     skinId.stringValue())));
      }

   }
}
