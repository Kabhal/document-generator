package com.tinubu.document.generator.openhtmltopdf.factory;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Objects.requireNonNull;

import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import com.openhtmltopdf.extend.FSStream;
import com.openhtmltopdf.extend.FSStreamFactory;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

public class DocumentStreamFactory implements FSStreamFactory {
   /**
    * URI base to use to prefix relative resources if any. Absolute resources won't be prefixed.
    *
    * @implNote Extra {@code /} is added to disambiguate path from authority URI component.
    */
   private static final String BASE_DOCUMENT_URI = "document:///";

   private final DocumentRepository documentRepository;
   private final DocumentProcessor documentProcessor;

   /**
    * Creates document factory.
    *
    * @param documentRepository document repository to search resource documents into
    * @param documentProcessor optional processor to apply to resource documents
    */
   public DocumentStreamFactory(DocumentRepository documentRepository, DocumentProcessor documentProcessor) {
      this.documentRepository = notNull(documentRepository, "documentRepository");
      this.documentProcessor = documentProcessor;
   }

   /**
    * Returns URI base to use to prefix relative resources.
    *
    * @return URI base to use to prefix relative resources
    */
   public static String baseDocumentUri() {
      return BASE_DOCUMENT_URI;
   }

   @Override
   public FSStream getUrl(String url) {
      return new FSStream() {
         @Override
         public InputStream getStream() {
            return documentRepository
                  .findDocumentById(DocumentPath.of(relativeDocumentPath(url))).map(document -> {
                     try {
                        return nullable(documentProcessor).map(document::process).orElse(document);
                     } catch (Exception e) {
                        document.content().close();
                        throw e;
                     }
                  })
                  .map(document -> document.content().inputStreamContent())
                  .orElse(null);
         }

         @Override
         public Reader getReader() {
            return documentRepository
                  .findDocumentById(DocumentPath.of(relativeDocumentPath(url)))
                  .map(document -> nullable(documentProcessor).map(document::process).orElse(document))
                  .map(document -> document.content().readerContent(StandardCharsets.UTF_8))
                  .orElse(null);
         }
      };
   }

   private Path relativeDocumentPath(String uri) {
      return Path.of("/").relativize(Path.of(requireNonNull(URI.create(uri).getPath())));
   }
}
