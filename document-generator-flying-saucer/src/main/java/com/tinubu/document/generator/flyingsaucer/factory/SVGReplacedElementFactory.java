package com.tinubu.document.generator.flyingsaucer.factory;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.util.SVGConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.resource.XMLResource;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Jpeg;

/**
 * Code from : <a
 * href="https://web.archive.org/web/20140418210449/http://www.samuelrossille.com/home/render-html-with-svg-to-pdf-with-flying-saucer.html">www.samuelrossille.com</a>
 */
public class SVGReplacedElementFactory implements ReplacedElementFactory {
   @Override
   public ReplacedElement createReplacedElement(LayoutContext c,
                                                BlockBox box,
                                                UserAgentCallback uac,
                                                int cssWidth,
                                                int cssHeight) {
      Element element = box.getElement();
      boolean isSvgImageTag =
            "img".equals(element.getNodeName()) && element.getAttribute("src").endsWith(".svg");

      if (isSvgImageTag) {
         XMLResource svgXml = uac.getXMLResource(element.getAttribute("src"));
         Document svgDocument = svgXml.getDocument();

         try {
            int width = box.getContentWidth() - 20;
            return rasterize(svgDocument, width);
         } catch (IOException e) {
            return null;
         }
      }
      return null;
   }

   public static ITextImageElement rasterize(Document dom, int width) throws IOException {

      TranscodingHints transcoderHints = new TranscodingHints();
      transcoderHints.put(ImageTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE);
      transcoderHints.put(ImageTranscoder.KEY_BACKGROUND_COLOR, Color.WHITE);
      transcoderHints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION,
                          SVGDOMImplementation.getDOMImplementation());
      transcoderHints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI, SVGConstants.SVG_NAMESPACE_URI);
      transcoderHints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT, "svg");
      transcoderHints.put(ImageTranscoder.KEY_WIDTH, (float) (2 * width));
      transcoderHints.put(ImageTranscoder.KEY_HEIGHT, (float) (2 * width));
      transcoderHints.put(ImageTranscoder.KEY_MAX_HEIGHT, (float) (2 * width));
      transcoderHints.put(ImageTranscoder.KEY_MAX_WIDTH, (float) (2 * width));

      try (ByteArrayOutputStream imageDataStream = new ByteArrayOutputStream()) {

         TranscoderInput input = new TranscoderInput(dom);
         TranscoderOutput output = new TranscoderOutput(imageDataStream);

         JPEGTranscoder t = new JPEGTranscoder();
         t.setTranscodingHints(transcoderHints);
         t.transcode(input, output);

         return new ITextImageElement(new ITextFSImage(new Jpeg(imageDataStream.toByteArray())));

      } catch (TranscoderException | BadElementException ex) {
         throw new IOException("Couldn't convert SVG");
      }
   }

   @Override
   public void reset() {
   }

   @Override
   public void remove(Element e) {
   }

   @Override
   public void setFormSubmissionListener(FormSubmissionListener listener) {
   }
}
