package com.tinubu.document.generator.flyingsaucer.generator;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.pdf.ITextOutputDevice;
import org.xhtmlrenderer.pdf.ITextUserAgent;
import org.xhtmlrenderer.util.XRLog;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

/**
 * Flying saucer {@link UserAgentCallback} for {@link DocumentRepository}.
 */
public class DocumentRepositoryUserAgent extends ITextUserAgent {

   private final DocumentRepository resourceRepository;
   private final DocumentProcessor documentProcessor;

   public DocumentRepositoryUserAgent(ITextOutputDevice outputDevice,
                                      DocumentRepository resourceRepository,
                                      DocumentProcessor documentProcessor) {
      super(outputDevice);
      this.resourceRepository = notNull(resourceRepository, "resourceRepository");
      this.documentProcessor = documentProcessor;
   }

   public DocumentRepositoryUserAgent(ITextOutputDevice outputDevice, DocumentRepository resourceRepository) {
      this(outputDevice, resourceRepository, null);
   }

   @Override
   protected InputStream resolveAndOpenStream(String uri) {
      try {
         URI result = new URI(uri);
         if (result.getScheme() != null || Paths.get(uri).isAbsolute()) {
            return super.resolveAndOpenStream(uri);
         }
      } catch (URISyntaxException e) {
         XRLog.exception("Invalid URI : " + uri, e);
         return null;
      }

      return resourceRepository
            .findDocumentById(DocumentPath.of(uri)).map(document -> {
               try {
                  return nullable(documentProcessor).map(document::process).orElse(document);
               } catch (Exception e) {
                  document.content().close();
                  throw e;
               }
            })
            .map(document -> document.content().inputStreamContent())
            .orElseGet(() -> super.resolveAndOpenStream(uri));
   }

   @Override
   public void setBaseURL(String uri) {
      if (uri == null) {
         super.setBaseURL(".");
      } else {
         try {
            URI baseUrl = new URI(uri);
            if (baseUrl.getScheme() != null || Paths.get(uri).isAbsolute()) {
               super.setBaseURL(uri);
            } else {
               super.setBaseURL(normalizeURI(baseUrl).toString());
            }
         } catch (URISyntaxException e) {
            XRLog.exception("Invalid URI : " + uri, e);
         }
      }
   }

   /**
    * If URI has a scheme (file:, http:, ...), this resolver is totally bypassed, and regular resolver is
    * used.
    * Otherwise, paths are normalized, absolute paths are relativized to root.
    *
    * @param uri A URI, possibly relative.
    *
    * @return resolved URI
    */
   @Override
   public String resolveURI(String uri) {
      if (uri == null) {
         return null;
      }

      try {
         URI resolvedUri = new URI(uri);
         URI baseUrl = new URI(getBaseURL());
         if (baseUrl.getScheme() != null || resolvedUri.getScheme() != null || Paths.get(uri).isAbsolute()) {
            return super.resolveURI(uri);
         }

         return normalizePath(Paths
                                    .get(baseUrl.getPath())
                                    .resolve(Paths.get(resolvedUri.getPath()))).toString();
      } catch (URISyntaxException e) {
         XRLog.exception("Invalid URI : " + uri, e);
         return null;
      }
   }

   private Path normalizeURI(URI uri) {
      return normalizePath(Paths.get(uri.getPath()));
   }

   private Path normalizePath(Path path) {
      Path normalizedPath = path.normalize();
      if (normalizedPath.isAbsolute()) {
         normalizedPath = Paths.get("/").relativize(normalizedPath);
      }
      return normalizedPath;
   }

}