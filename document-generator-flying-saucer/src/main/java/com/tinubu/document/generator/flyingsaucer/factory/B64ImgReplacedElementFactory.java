package com.tinubu.document.generator.flyingsaucer.factory;

import java.io.IOException;
import java.util.Base64;

import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;

/**
 * Custom ReplacedElementFactory used to generate PDF document with flying saucer.
 * Flying Saucer doesn't support embedding base64 images in 'img' tag.
 * References
 * <a href="https://code.google.com/archive/p/flying-saucer/issues/193">Issue</a>
 * <a href="https://gist.github.com/fengyie007/7575671">gist</a>
 *
 * @implNote Base64 decoder try sequentially to use basic URL, and MIME Base64 alphabets.
 */
public class B64ImgReplacedElementFactory implements ReplacedElementFactory {

   public ReplacedElement createReplacedElement(LayoutContext c,
                                                BlockBox box,
                                                UserAgentCallback uac,
                                                int cssWidth,
                                                int cssHeight) {
      Element e = box.getElement();
      if (e == null) {
         return null;
      }
      String nodeName = e.getNodeName();
      if (nodeName.equals("img")) {
         String attribute = e.getAttribute("src");
         FSImage fsImage;
         try {
            fsImage = buildImage(attribute, uac);
         } catch (BadElementException | IOException e1) {
            fsImage = null;
         }
         if (fsImage != null) {
            if (cssWidth != -1 || cssHeight != -1) {
               fsImage.scale(cssWidth, cssHeight);
            }
            return new ITextImageElement(fsImage);
         }
      }
      return null;
   }

   protected FSImage buildImage(String srcAttr, UserAgentCallback uac)
         throws IOException, BadElementException {
      FSImage fsImage;
      if (srcAttr.startsWith("data:image/")) {
         String b64encoded = srcAttr.substring(srcAttr.indexOf("base64,") + "base64,".length());

         byte[] decodedBytes;
         try {
            decodedBytes = Base64.getDecoder().decode(b64encoded);
         } catch (IllegalArgumentException e) {
            try {
               decodedBytes = Base64.getUrlDecoder().decode(b64encoded);
            } catch (IllegalArgumentException e2) {
               decodedBytes = Base64.getMimeDecoder().decode(b64encoded);
            }

         }
         fsImage = new ITextFSImage(Image.getInstance(decodedBytes));
      } else {
         fsImage = uac.getImageResource(srcAttr).getImage();
      }
      return fsImage;
   }

   @Override
   public void setFormSubmissionListener(FormSubmissionListener formSubmissionListener) {
      // empty body
   }

   @Override
   public void reset() {
      // empty body
   }

   @Override
   public void remove(Element element) {
      // empty body
   }
}