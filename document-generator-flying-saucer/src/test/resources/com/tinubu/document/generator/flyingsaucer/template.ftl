<#-- @ftlvariable name="skinId" type="com.tinubu.commons.ports.document.domain.DocumentPath" -->
<#-- @ftlvariable name="headerId" type="com.tinubu.commons.ports.document.domain.DocumentPath" -->
<#-- @ftlvariable name="footerId" type="com.tinubu.commons.ports.document.domain.DocumentPath" -->
<html lang="EN">
<head>
   <title>Welcome!</title>
    <#if skinId??>
    <#else>
       <link rel="stylesheet" href="skin/styles.css">
    </#if>
   <style>
      <#if skinId??>
      <#include "${skinId.stringValue()}">
      </#if>
      <#if watermarkId??>
      body {
         background-image: url('${watermarkId.stringValue()}');
         background-repeat: no-repeat; background-attachment: fixed; padding: 0px;
      }
      </#if>
   </style>
</head>
<body>
<div class="template-body">
    <#if headerId??><#include "${headerId.stringValue()}"></#if>
   <h1>Simple template</h1>
   <p> With plain text to test.</p>
</div>
</body>
</html>