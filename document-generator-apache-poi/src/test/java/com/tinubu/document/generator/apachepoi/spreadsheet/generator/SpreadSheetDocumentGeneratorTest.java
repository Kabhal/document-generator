package com.tinubu.document.generator.apachepoi.spreadsheet.generator;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_XLSX;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.document.generator.apachepoi.spreadsheet.generator.SpreadSheetDocumentGenerator.ModelSheetVisibility.VISIBLE;
import static com.tinubu.document.generator.core.support.DebugUtils.openDocument;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.document.generator.apachepoi.spreadsheet.generator.SpreadSheetDocumentGenerator.ModelSheetVisibility;
import com.tinubu.document.generator.apachepoi.spreadsheet.generator.SpreadSheetDocumentGenerator.SpreadSheetDocumentGeneratorBuilder;
import com.tinubu.document.generator.apachepoi.spreadsheet.processor.ModelTransferProcessor;
import com.tinubu.document.generator.core.environment.ConfigurableEnvironment.ConfigurableEnvironmentBuilder;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.DocumentGenerator;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.model.SpreadsheetModel.SpreadsheetModelBuilder;
import com.tinubu.document.generator.core.model.SpreadsheetModel.Table;
import com.tinubu.document.generator.core.skin.NoopSkin;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate.SpreadsheetTemplateBuilder;

class SpreadSheetDocumentGeneratorTest {

   @Test
   void generateXlsxDocumentWhenModelReferencedByFunction() throws DocumentGenerationException {
      DocumentGenerator generator = new SpreadSheetDocumentGeneratorBuilder(environment()).build();

      try (GeneratedDocument xlsx = generator
            .generateFromTemplate(template("test_model_function.xlsx"),
                                  new SpreadsheetModelBuilder().tables(dataModel()).build(),
                                  noopSkin())
            .loadDocumentContent()) {
         Document document = xlsx.document();

         assertThat(document.documentId().stringValue()).matches("test_model_function-.+\\.xlsx");
         assertThat(document.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_XLSX);
         assertThat(document.content().content()).isNotEmpty();
         assertThat(document.metadata().documentName()).matches("test_model_function-.+\\.xlsx");

         openDocument(xlsx);
      }
   }

   @Test
   void generateXlsxDocumentWhenModelReferencedByFormula() throws DocumentGenerationException {
      DocumentGenerator generator = new SpreadSheetDocumentGeneratorBuilder(environment()).build();

      try (GeneratedDocument xlsx = generator
            .generateFromTemplate(template("test_model_formula.xlsx"),
                                  new SpreadsheetModelBuilder().tables(dataModel()).build(),
                                  noopSkin())
            .loadDocumentContent()) {
         Document document = xlsx.document();

         assertThat(document.documentId().stringValue()).matches("test_model_formula-.+\\.xlsx");
         assertThat(document.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_XLSX);
         assertThat(document.content().content()).isNotEmpty();
         assertThat(document.metadata().documentName()).matches("test_model_formula-.+\\.xlsx");

         openDocument(xlsx);
      }
   }

   @Test
   void generateXlsxDocumentWhenModelReferencedByRowColumn() throws DocumentGenerationException {
      DocumentGenerator generator = new SpreadSheetDocumentGeneratorBuilder(environment())
            .modelSheetsVisibility(ModelSheetVisibility.NONE)
            .build();

      try (GeneratedDocument xlsx = generator
            .generateFromTemplate(template("test_model.xlsx"),
                                  new SpreadsheetModelBuilder().tables(dataModel()).build(),
                                  noopSkin())
            .loadDocumentContent()) {
         Document document = xlsx.document();

         assertThat(document.documentId().stringValue()).matches("test_model-.+\\.xlsx");
         assertThat(document.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_XLSX);
         assertThat(document.content().content()).isNotEmpty();
         assertThat(document.metadata().documentName()).matches("test_model-.+\\.xlsx");

         openDocument(xlsx);
      }
   }

   @Test
   void convertDocumentWhenNominal() throws DocumentGenerationException {
      DocumentGenerator generator = new SpreadSheetDocumentGeneratorBuilder(environment()).build();

      try (var xlsx = generator
            .generateFromTemplate(template("test_model_function.xlsx"),
                                  new SpreadsheetModelBuilder().tables(dataModel()).build(),
                                  noopSkin())
            .loadDocumentContent(); //
           var convertedDocument = generator.convert(xlsx)) {

         Document document = convertedDocument.document();

         assertThat(document.documentId().stringValue()).matches("test_model_function-.+\\.xlsx");
         assertThat(document.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_XLSX);
         assertThat(document.content().content()).isNotEmpty();
         assertThat(document.metadata().documentName()).matches("test_model_function-.+\\.xlsx");

         openDocument(convertedDocument);
      }
   }

   @Test
   void shouldThrowTooManyRowsException() {
      DocumentGenerator generator = new SpreadSheetDocumentGeneratorBuilder(environment())
            .modelSheetsVisibility(VISIBLE)
            .postProcessor(new ModelTransferProcessor() {
               @Override
               public void transferModel(Workbook workbook, SpreadsheetModel model) {
                  Table defaultTable = Table.of(new Object[10][10]);
                  transferModel(sheet(workbook, 0), defaultTable, 1048567, 10);
               }
            })
            .build();

      assertThatExceptionOfType(DocumentGenerationException.class)
            .isThrownBy(() -> generator.generateFromTemplate(template("test_model.xlsx"),
                                                             spreadsheetModel(),
                                                             noopSkin()))
            .withMessage("You provide 10 rows which exceeds 9 equal to maximum limit of rows");
   }

   @Test
   void shouldThrowTooManyColumnsException() {
      DocumentGenerator generator = new SpreadSheetDocumentGeneratorBuilder(environment())
            .modelSheetsVisibility(VISIBLE)
            .postProcessor(new ModelTransferProcessor() {
               @Override
               public void transferModel(Workbook workbook, SpreadsheetModel model) {
                  Table defaultTable = Table.of(new Object[10][10]);
                  transferModel(sheet(workbook, 0), defaultTable, 10, 16380);
               }
            })
            .build();

      assertThatExceptionOfType(DocumentGenerationException.class)
            .isThrownBy(() -> generator.generateFromTemplate(template("test_model.xlsx"),
                                                             spreadsheetModel(),
                                                             noopSkin()))
            .withMessage("You provide 10 columns which exceeds 4 equal to maximum limit of columns");
   }

   @TestInstance(Lifecycle.PER_CLASS)
   @Nested
   class NestedParameterizedTest {
      @ParameterizedTest
      @MethodSource("indexes")
      void generateSpreadDocumentWithTooManyRows_ShouldThrowException(int rows, int columns, String prefix) {
         var generator = new SpreadSheetDocumentGeneratorBuilder().preProcessor(new ModelTransferProcessor() {
            @Override
            public void transferModel(Workbook workbook, SpreadsheetModel model) {
               Table defaultTable = model.table("default").orElseThrow();
               transferModel(sheet(workbook, 0), defaultTable.subTable(rows, columns));
            }
         }).build();
         assertThatExceptionOfType(DocumentGenerationException.class)
               .isThrownBy(() -> generator.generateFromTemplate(template("test_model.xlsx"),
                                                                spreadsheetModel(),
                                                                noopSkin()))
               .withMessage(String.format("'%s' must be included in ]0, 10]", prefix));
      }

      private Stream<Arguments> indexes() {
         return Stream.of(arguments(11, 23, "rows"), arguments(10, 14, "columns"));
      }

      @ParameterizedTest
      @MethodSource("maps")
      void generateSpreadDocumentWithWrongData_ShouldThrowException(Map<String, Table> data,
                                                                    String description) {
         var generator = new SpreadSheetDocumentGeneratorBuilder().build();
         assertThatExceptionOfType(DocumentGenerationException.class)
               .isThrownBy(() -> generator.generateFromTemplate(template("test_model_formula.xlsx"),
                                                                new SpreadsheetModelBuilder()
                                                                      .tables(data)
                                                                      .build(),
                                                                noopSkin()))
               .as(description)
               .withMessage("Invalid sheetIndex: -1.");
      }

      private Stream<Arguments> maps() {
         return Stream.of(arguments(map(), "You must provide spreadsheet table(s)"),
                          arguments(map(entry("wrongName", Table.of(new Object[10][10]))),
                                    "You must provide spreadsheet table(s) with same sheet name as in your spreadsheet"));
      }
   }

   private NoopSkin noopSkin() {
      return Skin.noopSkin();
   }

   private SpreadsheetModel spreadsheetModel() {
      return new SpreadsheetModelBuilder()
            .tables(map(entry("default", Table.of(new Object[10][10]))))
            .build();
   }

   private SpreadsheetTemplate template(String path) {
      return new SpreadsheetTemplateBuilder()
            .templateRepository(templateRepository())
            .template(DocumentPath.of(path))
            .build();
   }

   private Environment environment() {
      return ConfigurableEnvironmentBuilder
            .ofEnvironment(SystemEnvironment.ofSystemDefaults())
            .timeZone(ZoneId.of("Europe/Paris"))
            .locale(Locale.FRANCE)
            .documentEncoding(StandardCharsets.UTF_8)
            .build();
   }

   private static ClasspathDocumentRepository templateRepository() {
      return new ClasspathDocumentRepository(Path.of(
            "/com/tinubu/document/generator/apachepoi/spreadsheet/generator"));
   }

   private Map<String, Table> dataModel() {
      final Object[][] defaultDataArray = {
            { "Header1", "Header2", "Header3", "Header4" },
            { "r1 c1", "r1 c2", 1, BigDecimal.valueOf(23.32) },
            { "r2 c1", "r2 c2", 2, BigDecimal.valueOf(3.2) },
            { "r3 c1", "r3 c2", 3, BigDecimal.valueOf(4.554) },
            { "r4 c1", "r4 c2", 4, BigDecimal.valueOf(103.3455) } };

      final Object[][] parametersDataArray = {
            { "Parameter", "Value" },
            { "string", "value" },
            { "date", new Date(ApplicationClock.nowAsInstant().toEpochMilli()) },
            {
                  "calendar",
                  peek(optional(Calendar.getInstance()),
                       c -> c.setTimeInMillis(ApplicationClock.nowAsInstant().toEpochMilli())).orElse(null) },
            { "number", BigDecimal.valueOf(103.3455) } };

      return map(entry("default", Table.of(defaultDataArray)),
                 entry("parameters", Table.of(parametersDataArray)));
   }

}