package com.tinubu.document.generator.apachepoi.spreadsheet.generator;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import org.apache.poi.ss.formula.OperationEvaluationContext;
import org.apache.poi.ss.formula.eval.BlankEval;
import org.apache.poi.ss.formula.eval.BoolEval;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.formula.eval.EvaluationException;
import org.apache.poi.ss.formula.eval.NumberEval;
import org.apache.poi.ss.formula.eval.OperandResolver;
import org.apache.poi.ss.formula.eval.StringEval;
import org.apache.poi.ss.formula.eval.ValueEval;
import org.apache.poi.ss.formula.functions.FreeRefFunction;

import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.model.SpreadsheetModel.Table;

public class ModelFunction implements FreeRefFunction {

   private static final String FUNCTION_NAME = "model";

   private final SpreadsheetModel model;

   public ModelFunction(SpreadsheetModel model) {
      this.model = notNull(model, "model");
   }

   public String name() {
      return FUNCTION_NAME;
   }

   @Override
   public ValueEval evaluate(ValueEval[] args, OperationEvaluationContext ec) {
      if (args.length < 2 || args.length > 3) {
         return ErrorEval.VALUE_INVALID;
      }

      try {
         int row;
         int column;
         String tableName = null;
         if (args.length == 3) {
            tableName = OperandResolver.coerceValueToString(args[0]);
            row = OperandResolver.coerceValueToInt(args[1]);
            column = OperandResolver.coerceValueToInt(args[2]);
         } else {
            row = OperandResolver.coerceValueToInt(args[0]);
            column = OperandResolver.coerceValueToInt(args[1]);
         }

         Table table = nullable(tableName)
               .map(model::table)
               .orElse(model.defaultTable())
               .orElseThrow(EvaluationException::invalidValue);
         Object value = table.data(row, column);

         return valueEval(value);
      } catch (EvaluationException e) {
         return e.getErrorEval();
      } catch (RuntimeException e) {
         return ErrorEval.REF_INVALID;
      }
   }

   private ValueEval valueEval(Object value) {
      ValueEval valueEval;

      if (value == null) {
         valueEval = BlankEval.instance;
      } else if (value instanceof Boolean) {
         valueEval = BoolEval.valueOf((Boolean) value);
      } else if (value instanceof Number) {
         valueEval = new NumberEval(((Number) value).doubleValue());
      } else {
         valueEval = new StringEval(String.valueOf(value));
      }

      return valueEval;
   }
}