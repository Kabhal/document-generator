package com.tinubu.document.generator.apachepoi.spreadsheet.processor;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

import com.tinubu.document.generator.core.model.SpreadsheetModel;
import com.tinubu.document.generator.core.template.SpreadsheetTemplate;

/**
 * Replace placeholders in Spreadsheet template.
 * <p>
 * Supported formats are :
 * <ul>
 *    <li>{@code @{=<formula>}}</li>
 *    <li>{@code @{[$]<model>![$]<col>[$]<row>}}</li>
 * </ul>
 */
public class ReplacePlaceholdersProcessor extends AbstractWorkbookProcessor {

   private static final Pattern PLACEHOLDER_PATTERN =
         Pattern.compile("@\\{((?:=(.+))|(?:\\$?([^!]+)!\\$?([A-Z]+)\\$?([0-9]+)))}");

   @Override
   public Workbook process(Workbook workbook, SpreadsheetTemplate template, SpreadsheetModel model) {
      notNull(workbook, "workbook");
      notNull(template, "template");
      notNull(model, "model");

      return replaceModelPlaceholders(workbook, model);
   }

   private Workbook replaceModelPlaceholders(Workbook workbook, SpreadsheetModel model) {
      for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
         Sheet sheet = workbook.getSheetAt(i);

         for (Row row : sheet) {
            for (Cell cell : row) {
               if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                  String stringCellValue = cell.getStringCellValue();
                  if (stringCellValue.startsWith("@")) {
                     Matcher matcher = PLACEHOLDER_PATTERN.matcher(stringCellValue);
                     if (matcher.matches()) {
                        if (matcher.group(2) != null) {
                           String formula = matcher.group(2);
                           cell.setCellFormula(formula);
                        } else {
                           String tableName = matcher.group(3);
                           String tableRow = matcher.group(5);
                           String tableColumn = matcher.group(4);

                           model.table(tableName).ifPresent(table -> {
                              int tRow = Integer.parseInt(tableRow) - 1;
                              int tColumn = CellReference.convertColStringToIndex(tableColumn);

                              setCellValue(cell, table.data(tRow, tColumn));
                           });
                        }
                     }
                  }
               }
            }
         }
      }

      return workbook;
   }

}
