package com.tinubu.document.generator.core.model;

import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullKeys;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.lang.Boolean.TRUE;

import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Model for document-based templates.
 */
public class TextModel extends AbstractValue implements Model {
   protected final Map<String, Object> data;

   protected TextModel(TextModelBuilder textModelBuilder) {
      this.data = immutable(map(textModelBuilder.data));
   }

   @Override
   public Fields<? extends TextModel> defineDomainFields() {
      return Fields.<TextModel>builder().field("data", v -> v.data, hasNoNullKeys()).build();
   }

   /**
    * No-op {@link TextModel} instance.
    *
    * @return no-op model
    */
   public static TextModel noopModel() {
      class NoopModel extends TextModel {
         protected NoopModel() {
            super(new TextModelBuilder());
         }
      }

      return new NoopModel();
   }

   /**
    * Document model data as a map indexed by data name.
    *
    * @return document model data
    */
   @Getter
   public Map<String, Object> data() {
      return data;
   }

   /**
    * Returns {@code true} if specified flag exists in model and has value {@link Boolean#TRUE}.
    *
    * @param flag flag name
    *
    * @return {@code true} if specified flag is present in model
    */
   public boolean hasFlag(String flag) {
      return data.containsKey(flag) && TRUE.equals(data.get(flag));
   }

   /**
    * Returns {@code true} if specified data exists in model.
    *
    * @param key data name
    *
    * @return {@code true} if specified data is present in model
    */
   public boolean hasData(String key) {
      return data.containsKey(key);
   }

   /**
    * Returns data value if specified data exists in model.
    *
    * @param key data name
    *
    * @return data value or {@link Optional#empty} if specified data is not present in model
    */
   public Optional<Object> data(String key) {
      return nullable(data.get(key));
   }

   /**
    * Returns data value if specified data exists in model, otherwise throw an exception.
    *
    * @param key data name
    *
    * @return data value
    *
    * @throws IllegalArgumentException if specified data is not present in model
    */
   public Object requiredData(String key) {
      return data(key).orElseThrow(() -> new IllegalArgumentException(String.format("Unknown '%s' data",
                                                                                    key)));
   }

   public static TextModelBuilder reconstituteBuilder() {
      return new TextModelBuilder().reconstitute();
   }

   public static class TextModelBuilder extends DomainBuilder<TextModel> {
      private Map<String, Object> data = map();

      public static TextModelBuilder from(TextModel model) {
         return new TextModelBuilder().<TextModelBuilder>reconstitute().data(model.data());
      }

      @Setter
      public TextModelBuilder data(Map<String, Object> data) {
         this.data = map(data);
         return this;
      }

      public TextModelBuilder addData(String key, Object value) {
         this.data.put(key, value);
         return this;
      }

      @Override
      public TextModel buildDomainObject() {
         return new TextModel(this);
      }
   }
}
