package com.tinubu.document.generator.core.skin;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.Reader;
import java.io.StringReader;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

public class CssSkin extends AbstractValue implements Skin {
   protected final Document skin;
   protected final DocumentRepository skinRepository;
   protected final DocumentPath logoId;
   protected final DocumentPath watermarkId;

   protected CssSkin(CssSkinBuilder builder) {
      this.skin = builder.skin;
      this.skinRepository = builder.skinRepository;
      this.logoId = builder.logoId;
      this.watermarkId = builder.watermarkId;
   }

   @SuppressWarnings("unchecked")
   public Fields<? extends CssSkin> defineDomainFields() {
      return Fields.<CssSkin>builder().superFields((Fields<CssSkin>) Skin.super.defineDomainFields()).build();
   }

   @Override
   public InvariantRule<MimeType> isSupportedSkinContentType() {
      return isTypeAndSubtypeEqualTo(value(TEXT_CSS));
   }

   @Getter
   @Override
   public Optional<Document> skin() {
      return nullable(skin);
   }

   @Getter
   @Override
   public Optional<DocumentRepository> skinRepository() {
      return nullable(skinRepository);
   }

   @Getter
   public Optional<DocumentPath> logoId() {
      return nullable(logoId);
   }

   @Getter
   public Optional<DocumentPath> watermarkId() {
      return nullable(watermarkId);
   }

   /**
    * No-op {@link CssSkin} instance.
    *
    * @return no-op skin
    */
   public static CssSkin noopSkin() {
      class NoopSkin extends CssSkin {
         protected NoopSkin() {
            super(new CssSkinBuilder().finalizeBuild());
         }
      }

      return new NoopSkin();
   }

   public static CssSkinBuilder reconstituteBuilder() {
      return new CssSkinBuilder().reconstitute();
   }

   public static class CssSkinBuilder extends DomainBuilder<CssSkin> {
      protected DocumentRepository skinRepository;
      protected Document skin;
      protected DocumentPath logoId;
      protected DocumentPath watermarkId;

      @Setter
      public CssSkinBuilder skinRepository(DocumentRepository skinRepository) {
         this.skinRepository = skinRepository;
         return this;
      }

      @Setter
      public CssSkinBuilder skin(Document skin) {
         this.skin = skin;
         return this;
      }

      public CssSkinBuilder skin(DocumentPath skinId) {
         return skin(findSkinById(skinId));
      }

      public CssSkinBuilder skinContent(String skinContent) {
         return skinContent(new StringReader(notNull(skinContent, "skinContent")));
      }

      public CssSkinBuilder skinContent(Reader skinContent) {
         return skin(createCssDocument("skin", skinContent));
      }

      @Setter
      public CssSkinBuilder logoId(DocumentPath logoId) {
         this.logoId = logoId;
         return this;
      }

      @Setter
      public CssSkinBuilder watermarkId(DocumentPath watermarkId) {
         this.watermarkId = watermarkId;
         return this;
      }

      @Override
      public CssSkin buildDomainObject() {
         return new CssSkin(this);
      }

      @Override
      @SuppressWarnings("unchecked")
      protected CssSkinBuilder finalizeBuild() {
         return super.finalizeBuild();
      }

      protected Document createCssDocument(String name, Reader content) {
         notBlank(name, "name");
         notNull(content, "content");

         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(name + "-" + Uuid
                                          .newNameBasedSha1Uuid(name)
                                          .stringValue()))
                                    .contentType(TEXT_CSS)
                                    .streamContent(content, UTF_8)
                                    .build());
      }

      private Document findSkinById(DocumentPath SkinId) {
         notNull(SkinId, "SkinId");
         notNull(skinRepository, "skinRepository");

         return skinRepository
               .findDocumentById(SkinId)
               .orElseThrow(() -> new IllegalArgumentException(String.format(
                     "Can't find '%s' skin in repository",
                     SkinId.stringValue())));
      }

   }
}
