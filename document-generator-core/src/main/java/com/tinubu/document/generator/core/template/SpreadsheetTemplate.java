package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.typed.NoopDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocument;

public class SpreadsheetTemplate extends AbstractValue implements Template {

   protected final DocumentRepository templateRepository;
   protected final Document template;

   protected SpreadsheetTemplate(SpreadsheetTemplateBuilder builder) {
      this.templateRepository = builder.templateRepository;
      this.template = nullable(builder.template, NoopDocument.noop());
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends SpreadsheetTemplate> defineDomainFields() {
      return Fields
            .<SpreadsheetTemplate>builder()
            .superFields((Fields<SpreadsheetTemplate>) Template.super.defineDomainFields())
            .build();
   }

   @Getter
   @Override
   public Document template() {
      return template;
   }

   @Getter
   @Override
   public Optional<DocumentRepository> templateRepository() {
      return nullable(templateRepository);
   }

   public static class SpreadsheetTemplateBuilder extends DomainBuilder<SpreadsheetTemplate> {
      protected DocumentRepository templateRepository;
      protected Document template;

      public static SpreadsheetTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new SpreadsheetTemplateBuilder()
               .template(template.template())
               .templateRepository(template.templateRepository().orElse(null));
      }

      @SuppressWarnings("resource")
      public static SpreadsheetTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return SpreadsheetTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      @Setter
      public SpreadsheetTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         this.templateRepository = templateRepository;
         return this;
      }

      @Getter
      public Optional<Document> template() {
         return nullable(template);
      }

      @Setter
      public SpreadsheetTemplateBuilder template(Document template) {
         this.template = template;
         return this;
      }

      public SpreadsheetTemplateBuilder template(DocumentPath templateId) {
         return template(findTemplateById(templateId));
      }

      @Deprecated
      public SpreadsheetTemplateBuilder templateId(DocumentPath templateId) {
         return template(templateId);
      }

      @Override
      public SpreadsheetTemplate buildDomainObject() {
         return new SpreadsheetTemplate(this);
      }

      private Document findTemplateById(DocumentPath templateId) {
         notNull(templateId, "templateId");
         notNull(templateRepository, "templateRepository");

         return templateRepository
               .findDocumentById(templateId)
               .orElseThrow(() -> new IllegalArgumentException(String.format(
                     "Can't find '%s' template in repository",
                     templateId.stringValue())));
      }

   }
}
