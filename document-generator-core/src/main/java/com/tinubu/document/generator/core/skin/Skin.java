package com.tinubu.document.generator.core.skin;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isAlwaysValid;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Skin definitions for document generation.
 */
public interface Skin extends Value {

   /**
    * No-op {@link Skin} instance.
    *
    * @return no-op skin
    */
   static NoopSkin noopSkin() {
      return new NoopSkin();
   }

   /**
    * Returns optional skin document.
    *
    * @return skin document
    *
    * @apiNote Skin is optional here because some generators do not rely at all on a document, but
    *       rather on an alternative representation (e.g.: attributes, ...).
    */
   Optional<Document> skin();

   /**
    * Returns the optional logo document identifier.
    *
    * @return document identifier
    */
   Optional<DocumentPath> logoId();

   /**
    * Returns the optional watermark document identifier.
    *
    * @return document identifier
    */
   Optional<DocumentPath> watermarkId();

   /**
    * Returns an optional skin document repository that can contain the skin itself, but also some extra
    * resources referenced by main skin, depending on skin implementation
    *
    * @return skin documents repository
    */
   Optional<DocumentRepository> skinRepository();

   /**
    * Returns the logo document if any.
    *
    * @return logo document or {@link Optional#empty}
    */
   default Optional<Document> logoDocument() {
      return logoId().flatMap(logoId -> skinRepository().flatMap(skinRepository -> skinRepository.findDocumentById(
            logoId)));
   }

   /**
    * Returns the logo document entry if any.
    *
    * @return logo document entry or {@link Optional#empty}
    */
   default Optional<DocumentEntry> logoDocumentEntry() {
      return logoId().flatMap(logoId -> skinRepository().flatMap(skinRepository -> skinRepository.findDocumentEntryById(
            logoId)));
   }

   /**
    * Returns the watermark document if any.
    *
    * @return logo document or {@link Optional#empty}
    */
   default Optional<Document> watermarkDocument() {
      return watermarkId().flatMap(watermarkId -> skinRepository().flatMap(skinRepository -> skinRepository.findDocumentById(
            watermarkId)));
   }

   /**
    * Returns the watermark document entry if any.
    *
    * @return watermark document entry or {@link Optional#empty}
    */
   default Optional<DocumentEntry> watermarkDocumentEntry() {
      return watermarkId().flatMap(watermarkId -> skinRepository().flatMap(skinRepository -> skinRepository.findDocumentEntryById(
            watermarkId)));
   }

   default Fields<? extends Skin> defineDomainFields() {
      return Fields
            .<Skin>builder()
            .field("skin", Skin::skin, optionalValue(metadata(contentType(isSupportedSkinContentType()))))
            .field("logoId", Skin::logoId)
            .field("watermarkId", Skin::watermarkId)
            .field("skinRepository", Skin::skinRepository, isNotNull())
            .build();
   }

   /**
    * Returns invariant rule to validate skin document content-type. You should override this method in
    * skin implementations not relying on typed skin documents.
    */
   default InvariantRule<MimeType> isSupportedSkinContentType() {
      return isAlwaysValid();
   }

   /**
    * Closes resources owned by this skin. Ensures internal references to resources handlers are correctly
    * freed after skin is used, or in case of unexpected error.
    */
   default void closeResources() {
      skin().ifPresent(skin -> skin.content().close());
   }

}
