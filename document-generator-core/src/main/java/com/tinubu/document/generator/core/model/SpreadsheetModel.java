package com.tinubu.document.generator.core.model;

import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

public class SpreadsheetModel extends AbstractValue implements Model {
   private static final int TABLE_MAX_ROWS = 16_384;
   private static final int TABLE_MAX_COLUMNS = 16_384;
   private static final String DEFAULT_TABLE_NAME = "default";

   protected final Map<String, Table> tables;

   protected SpreadsheetModel(SpreadsheetModelBuilder builder) {
      this.tables = immutable(builder.tables);
   }

   @Override
   public Fields<? extends SpreadsheetModel> defineDomainFields() {
      return Fields.<SpreadsheetModel>builder().field("tables", v -> v.tables, hasNoNullElements()).build();
   }

   /**
    * No-op {@link SpreadsheetModel} instance.
    *
    * @return no-op model
    */
   public static SpreadsheetModel noopModel() {
      class NoopModel extends SpreadsheetModel {
         protected NoopModel() {
            super(new SpreadsheetModelBuilder());
         }
      }

      return new NoopModel();
   }

   /**
    * Data tables indexed by table name.
    *
    * @return model data
    */
   @Getter
   public Map<String, Table> tables() {
      return tables;
   }

   public Optional<Table> table(String name) {
      notBlank(name, "name");

      return nullable(tables.get(name));
   }

   public Optional<Table> defaultTable() {
      return nullable(tables.get(DEFAULT_TABLE_NAME));
   }

   public static SpreadsheetModelBuilder reconstituteBuilder() {
      return new SpreadsheetModelBuilder().reconstitute();
   }

   /**
    * Single data table represented as a {@code matrix[rows][columns]}.
    */
   public static class Table {

      protected final int rows;
      protected final int columns;
      protected final Object[][] data;

      protected Table(int rows, int columns, Object[][] data) {
         this.rows = validateRows(rows, TABLE_MAX_ROWS);
         this.columns = validateColumns(columns, TABLE_MAX_COLUMNS);
         this.data = notNull(data, "data");
      }

      public static Table ofEmpty(int rows, int columns) {
         return new Table(rows, columns, new Object[rows][columns]);
      }

      public static Table of(Object[][] data) {
         int rows = validateRows(data.length, TABLE_MAX_ROWS);
         int columns = validateColumns(rows > 0 ? data[0].length : 0, TABLE_MAX_COLUMNS);

         Object[][] dataCopy = new Object[rows][columns];
         for (int i = 0; i < rows; i++)
            System.arraycopy(data[i], 0, dataCopy[i], 0, columns);

         return new Table(rows, columns, dataCopy);
      }

      public static Table of(List<? extends List<Object>> data) {
         int rows = validateRows(data.size(), TABLE_MAX_ROWS);
         int columns = validateColumns(rows > 0 ? data.get(0).size() : 0, TABLE_MAX_COLUMNS);

         Object[][] dataCopy = new Object[rows][columns];
         for (int i = 0; i < rows; i++) {
            List<Object> rowData = data.get(i);
            for (int j = 0; j < columns; j++) {
               dataCopy[i][j] = rowData.get(j);
            }
         }

         return new Table(rows, columns, dataCopy);
      }

      public int rows() {
         return rows;
      }

      public int columns() {
         return columns;
      }

      public Object[][] data() {
         return data;
      }

      public Object data(int row, int column) {
         validateRow(row);
         validateColumn(column);

         return data[row][column];
      }

      public Table data(int row, int column, Object data) {
         validateRow(row);
         validateColumn(column);

         this.data[row][column] = data;
         return this;
      }

      public Table rowData(int row, List<Object> rowData) {
         validateRow(row);
         validateColumns(rowData.size(), columns);

         for (int j = 0; j < rowData.size(); j++) {
            this.data[row][j] = rowData.get(j);
         }
         return this;
      }

      public Table columnData(int column, List<Object> columnData) {
         validateColumn(column);
         validateRows(columnData.size(), rows);

         for (int i = 0; i < columnData.size(); i++) {
            this.data[i][column] = columnData.get(i);
         }
         return this;
      }

      public Table subTable(int startRow, int startColumn, int rows, int columns) {
         validateRow(startRow);
         validateColumn(startColumn);
         validateRows(rows, this.rows - startRow);
         validateColumns(columns, this.columns - startColumn);

         Object[][] dataCopy = new Object[rows][columns];
         int i = 0;
         int j = rows;
         while (j > 0) {
            System.arraycopy(data[startRow], startColumn, dataCopy[i], 0, columns);
            i++;
            startRow++;
            j--;
         }

         return new Table(rows, columns, dataCopy);
      }

      public Table subTable(int rows, int columns) {
         return subTable(0, 0, rows, columns);
      }

      public Table subTableElement(int row, int column) {
         return subTable(row, column, 1, 1);
      }

      public Table subTableRows(int startRow, int rows) {
         return subTable(startRow, 0, rows, this.columns);
      }

      public Table subTableRow(int row) {
         return subTableRows(row, 1);
      }

      public Table subTableColumns(int startColumn, int columns) {
         return subTable(0, startColumn, this.rows, columns);
      }

      public Table subTableColumn(int column) {
         return subTableColumns(column, 1);
      }

      protected static int validateRows(int rows, int maxRows) {
         return satisfies(rows,
                          r -> r > 0 && r <= maxRows,
                          "rows",
                          "must be included in ]0, " + maxRows + "]");
      }

      protected static int validateColumns(int columns, int maxColumns) {
         return satisfies(columns,
                          c -> c > 0 && c <= maxColumns,
                          "columns",
                          "must be included in ]0, " + maxColumns + "]");
      }

      protected int validateRow(int row) {
         return satisfies(row,
                          r -> r >= 0 && r < this.rows,
                          "row",
                          "must be included in [0, " + this.rows + "[");
      }

      protected int validateColumn(int column) {
         return satisfies(column,
                          c -> c >= 0 && c < this.columns,
                          "column",
                          "must be included in [0, " + this.columns + "[");
      }
   }

   public static class SpreadsheetModelBuilder extends DomainBuilder<SpreadsheetModel> {
      private Map<String, Table> tables = map();

      public static SpreadsheetModelBuilder from(SpreadsheetModel model) {
         return new SpreadsheetModelBuilder().<SpreadsheetModelBuilder>reconstitute().tables(model.tables());
      }

      @Setter
      public SpreadsheetModelBuilder tables(Map<String, Table> tables) {
         this.tables = map(tables);
         return this;
      }

      public SpreadsheetModelBuilder addTable(String name, Table table) {
         this.tables.put(name, table);
         return this;
      }

      public SpreadsheetModelBuilder addTable(Table table) {
         return addTable(DEFAULT_TABLE_NAME, table);
      }

      @Override
      public SpreadsheetModel buildDomainObject() {
         return new SpreadsheetModel(this);
      }
   }
}
