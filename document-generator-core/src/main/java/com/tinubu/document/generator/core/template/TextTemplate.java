package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static com.tinubu.commons.lang.util.CheckedRunnable.unchecked;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.Reader;
import java.io.StringReader;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.document.generator.core.generator.GeneratedDocument;

/**
 * Generic template for text-based documents.
 */
public class TextTemplate extends AbstractValue implements Template {

   protected final DocumentRepository templateRepository;
   protected final Document template;
   protected final Document header;
   protected final Document footer;

   protected TextTemplate(TextTemplateBuilder builder) {
      this.templateRepository = builder.templateRepository;
      this.template = builder.template;
      this.header = builder.header;
      this.footer = builder.footer;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends TextTemplate> defineDomainFields() {
      return Fields
            .<TextTemplate>builder()
            .superFields((Fields<TextTemplate>) Template.super.defineDomainFields())
            .field("header", v -> v.header)
            .field("footer", v -> v.footer)
            .build();
   }

   @Getter
   @Override
   public Optional<DocumentRepository> templateRepository() {
      return nullable(templateRepository);
   }

   @Getter
   @Override
   public Document template() {
      return template;
   }

   /**
    * Returns the optional header document.
    *
    * @return header document id or {@link Optional#empty()}
    */
   @Getter
   public Optional<? extends Document> header() {
      return nullable(header);
   }

   /**
    * Returns the optional footer document.
    *
    * @return footer document id or {@link Optional#empty()}
    */
   @Getter
   public Optional<? extends Document> footer() {
      return nullable(footer);
   }

   @Override
   public void closeResources() {
      unchecked(() -> smartFinalize(() -> template.content().close(),
                                    () -> nullable(header).ifPresent(header -> header.content().close()),
                                    () -> nullable(footer).ifPresent(footer -> footer.content().close())));
   }

   public static TextTemplateBuilder reconstituteBuilder() {
      return new TextTemplateBuilder().reconstitute();
   }

   public static class TextTemplateBuilder extends DomainBuilder<TextTemplate> {
      protected DocumentRepository templateRepository;
      protected Document template;
      protected Document header;
      protected Document footer;

      public static TextTemplateBuilder ofTemplate(Template template) {
         validate(template, "template", isNotNull()).orThrow();

         return new TextTemplateBuilder()
               .template(template.template())
               .templateRepository(template.templateRepository().orElse(null))
               .optionalChain(nullableInstanceOf(template, TextTemplateBuilder.class),
                              (b, d) -> b.header(d.header).footer(d.footer));
      }

      @SuppressWarnings("resource")
      public static TextTemplateBuilder ofGeneratedDocument(GeneratedDocument generatedDocument) {
         validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

         return TextTemplateBuilder.ofTemplate(generatedDocument.asTemplate());
      }

      protected TextTemplateBuilder processTemplate(DocumentProcessor processor) {
         validate(processor, "processor", isNotNull()).orThrow();
         validate(template,
                  "template",
                  isNotNull("template must be set before calling this operation")).orThrow();

         template = template.process(processor);

         return this;
      }

      @Getter
      public Optional<DocumentRepository> templateRepository() {
         return nullable(templateRepository);
      }

      @Setter
      public TextTemplateBuilder templateRepository(DocumentRepository templateRepository) {
         this.templateRepository = templateRepository;
         return this;
      }

      @Getter
      public Optional<Document> template() {
         return nullable(template);
      }

      @Setter
      public TextTemplateBuilder template(Document template) {
         this.template = template;
         return this;
      }

      public TextTemplateBuilder template(DocumentPath templateId) {
         return template(findTemplateById(templateId));
      }

      @Deprecated
      public TextTemplateBuilder templateId(DocumentPath templateId) {
         return template(templateId);
      }

      public TextTemplateBuilder templateContent(String templateContent) {
         return templateContent(new StringReader(notNull(templateContent, "templateContent")));
      }

      public TextTemplateBuilder templateContent(Reader templateContent) {
         return template(createTextDocument("template", templateContent));
      }

      @Getter
      public Optional<Document> header() {
         return nullable(header);
      }

      @Setter
      public TextTemplateBuilder header(Document header) {
         this.header = header;
         return this;
      }

      public TextTemplateBuilder header(DocumentPath headerId) {
         return header(findTemplateById(headerId));
      }

      public TextTemplateBuilder headerContent(String headerContent) {
         return headerContent(new StringReader(notNull(headerContent, "headerContent")));
      }

      public TextTemplateBuilder headerContent(Reader headerContent) {
         return header(createTextDocument("header", headerContent));
      }

      @Setter
      public TextTemplateBuilder footer(Document footer) {
         this.footer = footer;
         return this;
      }

      public TextTemplateBuilder footer(DocumentPath footerId) {
         return footer(findTemplateById(footerId));
      }

      public TextTemplateBuilder footerContent(String footerContent) {
         return footerContent(new StringReader(notNull(footerContent, "footerContent")));
      }

      public TextTemplateBuilder footerContent(Reader footerContent) {
         return footer(createTextDocument("footer", footerContent));
      }

      @Override
      public TextTemplate buildDomainObject() {
         return new TextTemplate(this);
      }

      protected Document createTextDocument(String name, Reader content) {
         notBlank(name, "name");
         notNull(content, "content");

         return autoExtension(new DocumentBuilder()
                                    .documentId(DocumentPath.of(name + "-" + Uuid
                                          .newNameBasedSha1Uuid(name)
                                          .stringValue()))
                                    .contentType(TEXT_PLAIN)
                                    .streamContent(content, UTF_8)
                                    .build());
      }

      private Document findTemplateById(DocumentPath templateId) {
         notNull(templateId, "templateId");
         notNull(templateRepository, "templateRepository");

         return templateRepository
               .findDocumentById(templateId)
               .orElseThrow(() -> new IllegalArgumentException(String.format(
                     "Can't find '%s' template in repository",
                     templateId.stringValue())));
      }

   }
}
