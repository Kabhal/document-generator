package com.tinubu.document.generator.core.template;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isAlwaysValid;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Template representation for generators.
 */
public interface Template extends Value {

   /**
    * Returns an optional template document repository that can contain the template itself, but
    * also some extra resources referenced by main template, depending on template implementation.
    *
    * @return template documents repository
    */
   Optional<DocumentRepository> templateRepository();

   /**
    * Returns the template document.
    *
    * @return template document
    */
   Document template();

   default Fields<? extends Template> defineDomainFields() {
      return Fields
            .<Template>builder()
            .field("template",
                   Template::template,
                   isNotNull().andValue(metadata(contentType(isSupportedTemplateContentType()))))
            .field("templateRepository", Template::templateRepository)
            .build();
   }

   /**
    * Returns invariant rule to validate template document content-type. You should override this method in
    * template implementations not relying on typed template documents.
    */
   default InvariantRule<MimeType> isSupportedTemplateContentType() {
      return isAlwaysValid();
   }

   /**
    * Closes resources owned by this template. Ensures internal references to resources handlers are correctly
    * freed after template is used, or in case of unexpected error.
    */
   default void closeResources() {
      template().content().close();
   }
}
