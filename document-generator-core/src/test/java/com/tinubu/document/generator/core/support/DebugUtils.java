package com.tinubu.document.generator.core.support;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_DOCBOOK;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OCTET_STREAM;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOCX;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_XLSX;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_ANY;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.document.generator.core.generator.GeneratedDocument;

public final class DebugUtils {

   private static final Logger log = LoggerFactory.getLogger(DebugUtils.class);

   private static final Map<MimeType, String> APPLICATIONS = map(entry(TEXT_HTML, "Safari"),
                                                                 entry(APPLICATION_PDF, "Preview"),
                                                                 entry(APPLICATION_DOCBOOK, "Preview"),
                                                                 entry(APPLICATION_OOXML_DOCX, "LibreOffice"),
                                                                 entry(APPLICATION_OOXML_XLSX,
                                                                       "LibreOffice"));

   /** Extra thread wait duration for application to load the document. */
   private static final Duration WAIT_FOR_APPLICATION = Duration.ofSeconds(2);
   private static final String DOCUMENT_CONTENT_SEPARATOR = "--------------------";

   private static boolean debugDocuments =
         Boolean.parseBoolean(System.getProperty("document-generator.debug-documents"));

   private DebugUtils() { }

   public static void debugDocuments(boolean debugDocuments) {
      DebugUtils.debugDocuments = debugDocuments;
   }

   public static String dumpDocument(Document document) {
      String content = "";
      if (log.isDebugEnabled()) {
         if (document.metadata().contentType().map(TEXT_ANY::includes).orElse(false)) {
            content = document.content().stringContent();
            log.debug("Document {} dump :\n{}", document.documentId(), contentSeparators(content));
         } else {
            log.warn("Unsupported {} content-type for document dump", document.metadata().contentType());
         }
      }

      return content;
   }

   private static String contentSeparators(String content) {
      return DOCUMENT_CONTENT_SEPARATOR + "\n" + content + "\n" + DOCUMENT_CONTENT_SEPARATOR;
   }

   public static String dumpDocument(GeneratedDocument generatedDocument) {
      return dumpDocument(generatedDocument.document());
   }

   public static void openDocument(Document document, String application) {
      if (debugDocuments) {
         try {
            File tmpFile =
                  File.createTempFile("document-generator-debug", "_" + document.metadata().documentName());

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
               try {
                  Thread.sleep(WAIT_FOR_APPLICATION.toMillis());
                  tmpFile.delete();
               } catch (InterruptedException e) {
                  Thread.currentThread().interrupt();
               }
            }));

            try (FileOutputStream file = new FileOutputStream(tmpFile)) {
               file.write(document.content().content());
            }

            log.debug("Opening '{}' document with '{}'", tmpFile.getAbsolutePath(), application);

            try {
               new ProcessBuilder()
                     .command("open", "-a", application, tmpFile.getAbsolutePath())
                     .start()
                     .waitFor();
            } catch (InterruptedException e) {
               Thread.currentThread().interrupt();
            }
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }
   }

   public static void openDocument(GeneratedDocument generatedDocument, String application) {
      openDocument(generatedDocument.document(), application);
   }

   public static void openDocument(Document document) {
      Optional<String> application = nullable(APPLICATIONS.get(document
                                                                     .metadata()
                                                                     .simpleContentType()
                                                                     .orElse(APPLICATION_OCTET_STREAM)));

      application.ifPresentOrElse(app -> openDocument(document, app),
                                  () -> log.debug("No application registered for {} content-type",
                                                  document.metadata().contentType()));
   }

   public static void openDocument(GeneratedDocument generatedDocument) {
      openDocument(generatedDocument.document());
   }

}
