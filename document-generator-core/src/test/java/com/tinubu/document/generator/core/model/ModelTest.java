package com.tinubu.document.generator.core.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ModelTest {

   @Test
   void noopModelTest() {
      var noopModel = Model.noopModel();
      assertThat(noopModel).isExactlyInstanceOf(NoopModel.class).hasAllNullFieldsOrProperties();
   }
}