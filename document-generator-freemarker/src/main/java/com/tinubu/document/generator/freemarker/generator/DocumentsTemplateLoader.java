package com.tinubu.document.generator.freemarker.generator;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;

import java.io.Reader;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.List;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

import freemarker.cache.TemplateLoader;

/**
 * Freemarker template loader for a pre-defined list of {@link Document} documents.
 *
 * @implSpec Returned documents are references, so that specified document list is
 *       {@link Document#loadContent() content-loaded for reuse.
 */
public class DocumentsTemplateLoader implements TemplateLoader {

   private final List<Document> documents;
   private final DocumentProcessor documentProcessor;

   public DocumentsTemplateLoader(List<Document> documents, DocumentProcessor documentProcessor) {
      this.documents = list(stream(noNullElements(documents, "documents")).map(Document::loadContent));
      this.documentProcessor = documentProcessor;
   }

   public DocumentsTemplateLoader(List<Document> documents) {
      this(documents, null);
   }

   @Override
   public Object findTemplateSource(String name) {
      return stream(documents)
            .filter(document -> document.documentId().equals(DocumentPath.of(name)))
            .findFirst()
            .map(document -> nullable(documentProcessor).map(document::process).orElse(document))
            .orElse(null);
   }

   @Override
   public long getLastModified(Object templateSource) {
      return ((Document) templateSource).metadata().lastUpdateDate().map(Instant::toEpochMilli).orElse(-1L);
   }

   @Override
   public Reader getReader(Object templateSource, String encoding) {
      return (((Document) templateSource).content().readerContent(Charset.forName(encoding)));
   }

   @Override
   public void closeTemplateSource(Object templateSource) {
   }
}
